<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', function () {
    return view('index');
});


Route::get('sendbasicemail','MailController@basic_email');
Route::get('sendhtmlemail','MailController@html_email');
Route::get('sendattachmentemail','MailController@attachment_email');


Route::group(['middleware' => ['reseller']], function () {

Route::get('/re_index', 'resellerController@re_index');
    
Route::get('/codes', 'resellerController@code');
Route::get('/search_code', 'resellerController@search_code');
Route::get('/send', 'resellerController@send');
Route::get('/receive', 'resellerController@receive');
Route::get('/demonds', 'resellerController@demond');
Route::get('/add_user', 'resellerController@add_user');
Route::get('/all_resellers', 'resellerController@all_resellers');
Route::get('/today', 'resellerController@today');
Route::get('/yesterday', 'resellerController@yesterday');
Route::get('/chargings', 'resellerController@charging');
Route::get('/packagess', 'resellerController@packages');
Route::get('/add_package', 'resellerController@add_package');
Route::get('/change_prices', 'resellerController@change_price');
Route::get('/change_code', 'resellerController@change_code');
Route::get('/being_sports', 'resellerController@being_sport');
Route::get('/pricess', 'resellerController@prices');
Route::post('/save_user','resellerController@save_user');
Route::post('/demand_sold','resellerController@demand_sold');
Route::post('approves/{id?}/{ids?}/{idd?}','resellerController@approves');
Route::get('/re_settings','resellerController@setting');
Route::post('/password_update','resellerController@password_update');
Route::get('r_reslller_info/{ids}' , 'resellerController@r_reslller_info');
Route::post('r_reseller_update/{id}','resellerController@r_reseller_update');
Route::post('reseller_send_sold','resellerController@reseller_send_sold');
Route::get('sub_cat_reseller/{id}','resellerController@sub_cat_reseller');
Route::post('reseller_order','resellerController@reseller_order');
Route::post('reseller_package','resellerController@reseller_package');
Route::get('ressller_pack','resellerController@ressller_pack');
Route::get('see_reseller_pack/{pr_id}','resellerController@see_reseller_pack');
Route::post('reseller_package_update/{pr_id}','resellerController@reseller_package_update');
Route::post('reseller_send_pack','resellerController@reseller_send_pack');
Route::Post('reseller_update_code/{cp_id}','resellerController@reseller_update_code');
Route::post('serch','resellerController@serch');
Route::get('statics','resellerController@statics');
Route::post('statics_post','resellerController@statics_post');


});

Route::get('/forget_password', 'adminController@forget_password');

Route::group(['middleware' => ['admin']], function () {
    
    Route::get('/send/email', 'HomeController@mail');
    Route::get('/wp-admin', 'adminController@admin');
    Route::get('/wp-admin/code', 'adminController@code'); 
    Route::get('/wp-admin/sold','adminController@sold');
    Route::get('/wp-admin/add-seller','adminController@add_seller');
    Route::get('/wp-admin/reseller', 'adminController@reseller');
    Route::get('/wp-admin/today', 'adminController@today');
    Route::get('/wp-admin/yesterday', 'adminController@yesterday');
    Route::get('/wp-admin/charging', 'adminController@charging');
    Route::get('/wp-admin/packages','adminController@packages');
    Route::get('/wp-admin/change-prices','adminController@change_prices');
    Route::get('/wp-admin/begin-sport','adminController@begin_sport');
    Route::get('/wp-admin/add-codes', 'adminController@add_codes');
    Route::post('/code_save', 'adminController@code_save');
    Route::post('/reseller_save', 'adminController@reseller_save');
    Route::post('/add_charging', 'adminController@add_charging');
    Route::get('/code_del/{id}', 'adminController@code_del');
    Route::post('admin_send_sold','adminController@admin_send_sold');
    Route::get('add_cat','adminController@add_cat')->name('add_cat');
    Route::get('sub_cat/{id}' , 'adminController@sub_cat');
    Route::post('add_sub_cat','adminController@add_sub_cat');
    Route::get('/admin_setting','adminController@admin_setting');
    Route::post('/admin_password_update','adminController@password_update');
    Route::get('priced','adminController@prices');
    Route::get('sub/{id}', 'adminController@sub');
    Route::get('add_new_packages','adminController@add_new_packages');
    Route::post('package','adminController@package');
    Route::get('your_pack','adminController@your_pack');
    Route::get('see_pack/{id}','adminController@see_pack');
    Route::post('pack_update/{p_id?}','adminController@pack_updates');
    Route::get('sub_cat_bs/{id}' , 'adminController@sub_cat_bs');
    Route::get('reslller_info/{id}' , 'adminController@reslller_info');
    Route::post('reseller_update/{id}','adminController@reseller_update');
    Route::post('admi_send_sold','adminController@admi_send_sold');
    Route::post('admin_send_pack','adminController@admin_send_pack');
    Route::post('subcat', 'adminController@subCat');
    Route::get('sub_cat_code','adminController@sub_cat_code');
    Route::get('disactive/{id}','adminController@disactive');
    Route::get('active/{id}','adminController@active');
    Route::get('delete_user/{id}','adminController@delete_user');
    Route::get('admin_reseller','adminController@admin_reseller');
    Route::get('sold_req','adminController@sold_req');
    Route::post('sold_req_approve/{id?}/{ids?}','adminController@sold_req_approve');
    Route::get('admin_sub_cat/{id}','adminController@admin_sub_cat');
    Route::post('advertisement','adminController@advertisement');
    Route::post('bein_sports','adminController@bein_sports');
    Route::post('sub_cat_pri','adminController@sub_cat_pri');
    Route::get('stock','adminController@stock');
    Route::get('code_stock/{ids}','adminController@code_stock');
    Route::get('stock_code_del/{Nid}','adminController@stock_code_del');
    Route::get('last200','adminController@last200');
    Route::get('code_dels/{Nid}','adminController@code_dels');
    Route::get('static','adminController@static');
    Route::post('static_post','adminController@static_post');
    Route::get('sold_his/{id}','adminController@sold_his');
    Route::post('admin_order_code','adminController@admin_order_code');
    Route::post('admin_update_code/{ad_id}','adminController@admin_update_code');
    Route::get('code_purchase_his','adminController@code_purchase_his');
    
});




Route::group(['middleware' => ['user']], function () {

    Route::get('/user_index', 'userController@user_index');
    Route::get('/dashboard', 'userController@dashboard');
    Route::get('/code', 'userController@code');
    Route::get('/search', 'userController@search');
    Route::get('/sold', 'userController@sold');
    Route::get('/demond', 'userController@demond');
    Route::get('/charging', 'userController@charging');
    Route::get('/being_sport', 'userController@being_sport');
    Route::get('/prices', 'userController@prices');
    Route::post('demand_solds','userController@demand_solds');
    Route::get('/settings','userController@setting');
    Route::post('/user_password_update','userController@password_update');
    Route::post('user_order','userController@user_order');
    Route::get('sub_cat_user/{ids}','userController@sub_cat_user');
    Route::post('user_update_code/{ucp_id}','userController@user_update_code');


});    

Route::post('admin_do_login','adminController@admin_do_login');
Route::get('admin_logout','adminController@admin_logout');



