@extends('user/user_index')
@section('main')active @endsection
@section('content')

<head>
<link rel="icon" href="assets/img/brand/favicon.png" type="image/png">
  <!-- Fonts -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
  <!-- Icons -->
  <link rel="stylesheet" href="assets/css/argon.css?v=1.2.0" type="text/css">
  <link rel="stylesheet" href="assets/vendor/nucleo/css/nucleo.css" type="text/css">
</head>
<style>
     .arrow {
    padding-left:60px;
} 

</style>
<div class="dashboard-wrapper">
            <div class="dashboard-ecommerce">
                <div class="container-fluid dashboard-content ">
                    <!-- ============================================================== -->
                    <!-- pageheader  -->
                    <!-- ============================================================== -->
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="page-header">
                                <h2 class="pageheader-title">Dashboard <small style="color:#777">Control Panel</small> </h2>
                                <p class="pageheader-text">Nulla euismod urna eros, sit amet scelerisque torton lectus vel mauris facilisis faucibus at enim quis massa lobortis rutrum.</p>
                                <div  class="page-breadcrumb">
                               
									<ol class="breadcrumb">
                                  <li class="active">
                                  <i class="fa fa-chart-pie"></i> Dashboard
                                   </li>
                                  </ol>
                                   
                                </div>
                            </div>
                        </div>
                    </div>


                    @foreach($add as $ad)
            @if(($ad->show)==0)
            <div class="alert alert-info alert-danger">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <i class="fa fa-info-circle"></i><strong>{{$ad->ads}}</strong>
            </div>
            @endif
            @endforeach
					      
          <div class="row">
            @if($DONGEL!=null)
            <div class="col-xl-4 col-md-6">
            
              @foreach($use1 as $row1)           
              <div class="card card-stats">
                <!-- Card body -->
                <div class="card-body" style="background-color:brown">
                  <div class="row">
                    <div class="col">
                     
                    <span class="text-success"><i class="fa fa-desktop fa-5x" style="float:left;color:white"></i></span>
                    <span class="text-nowrap"><h2  class="text-white">{{$row1->cat}}</h2></span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                        <i class="ni ni-active-40"></i>
                      </div>
                    </div>
                  </div>
                  </div>
                  <div style="background-color:#f5f5f5">
                  <p class="mt-3 mb-0 text-sm">
                  <a href="{{ url('sub_cat_user',['ids'=>$row1->id])}}">
                    <span class="text-nowrap text-black ml-2 px-4" >View Details</span>
                    <span class="text-success"><i class="fa fa-arrow-right text-#d9534f"></i></span>
                  </a>  
                </p>
                <br>
                  </div>
              </div>
              @endforeach 
            </div>
            @endif 
            @if($SERVERS!=null)       
            <div class="col-xl-4 col-md-6">
            @foreach($use2 as $row2)
              <div class="card card-stats">
                <!-- Card body -->
                <div class="card-body" style="background-color:green">
                  <div class="row">
                    <div class="col">
                        
                    <span class="text-success"><i class="fa fa-desktop fa-5x" style="float:left;color:white"></i></span>
                    <span class="text-nowrap"><h2 class="text-white">{{$row2->cat}}</h2></span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                        <i class="ni ni-active-40"></i>
                      </div>
                    </div>
                  </div>
                  </div>
                  <div style="background-color:#f5f5f5">
                  <p class="mt-3 mb-0 text-sm">
                  <a href="{{ url('sub_cat_user',['ids'=>$row2->id])}}">
                    <span class="text-nowrap text-black ml-2 px-4" >View Details</span>
                    <span class="text-success"><i class="fa fa-arrow-right text-#5cb85c"></i></span>
                    </a>
                </p>
                <br>
                  </div>
              </div>
              @endforeach
            </div>
            @endif
            @if($IPT!=null) 
            <div class="col-xl-4 col-md-6">
            @foreach($use3 as $row3)
              <div class="card card-stats">
                <!-- Card body -->
                <div class="card-body" style="background-color:blue">
                  <div class="row">
                    <div class="col">
                        
                    <span class="text-success"><i class="fa fa-desktop fa-5x" style="float:left;color:white"></i></span>
                    <span class="text-nowrap"><h2  class="text-white">{{$row3->cat}}</h2></span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                        <i class="ni ni-active-40"></i>
                      </div>
                    </div>
                  </div>
                  </div>
                  <div style="background-color:#f5f5f5">
                  <p class="mt-3 mb-0 text-sm">
                  <a href="{{ url('sub_cat_user',['ids'=>$row3->id])}}">
                    <span class="text-nowrap text-black ml-2 px-4" >View Details</span>
                    <span class="text-success"><i class="fa fa-arrow-right text-primary "></i></span>
                    </a>
                </p>
                <br>
                  </div>
              </div>
            @endforeach 
            
            </div> 
            @endif
            @if($VOD !=null)
            
            <div class="col-xl-4 col-md-6">
            @foreach($use4 as $row4) 
              <div class="card card-stats">
                <!-- Card body -->
                <div class="card-body" style="background-color:#f0ad4e">
                  <div class="row">
                    <div class="col">
                        
                    <span class="text-success"><i class="fa fa-desktop fa-5x" style="float:left;color:white"></i></span>
                    <span class="text-nowrap"><h2  class="text-white">{{$row4->cat}}</h2></span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                        <i class="ni ni-active-40"></i>
                      </div>
                    </div>
                  </div>
                  </div>
                  <div style="background-color:#f5f5f5">
                  <p class="mt-3 mb-0 text-sm">
                  <a href="{{ url('sub_cat_user',['ids'=>$row4->id])}}">
                    <span class="text-nowrap text-black ml-2 px-4" >View Details</span>
                    <span class="text-success"><i class="fa fa-arrow-right text-primary"></i></span>
                    </a>                
                  </p>
                <br>
                  </div>
              </div>
            @endforeach  
            </div>
            @endif
            
            
            @if($TEST!=null) 
            <div class="col-xl-4 col-md-6">
            @foreach($use5 as $row5)
              <div class="card card-stats">
                <!-- Card body -->
                <div class="card-body" style="background-color:blue">
                  <div class="row">
                    <div class="col">
                        
                    <span class="text-success"><i class="fa fa-flask fa-5x" style="float:left;color:white"></i></span>
                    <span class="text-nowrap"><h2  class="text-white">{{$row5->cat}}</h2></span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                        <i class="ni ni-active-40"></i>
                      </div>
                    </div>
                  </div>
                  </div>
                  <div style="background-color:#f5f5f5">
                  <p class="mt-3 mb-0 text-sm">
                  <a href="{{ url('sub_cat_user',['ids'=>$row5->id])}}">
                    <span class="text-nowrap text-black ml-2 px-4" >View Details</span>
                    <span class="text-success"><i class="fa fa-arrow-right text-primary"></i></span>
                    </a>
                </p>
                <br>
                  </div>
              </div>
              @endforeach   
            </div>
            @endif
            @if($GIFTCARDS!=null)  
            <div class="col-xl-4 col-md-6">
            @foreach($use6 as $row6)  
              <div class="card card-stats">
                <!-- Card body -->
                <div class="card-body" style="background-color:yellow">
                  <div class="row">
                    <div class="col">
                        
                    <span class="text-success"><i class="fa fa-gift fa-5x" style="float:left;color:white"></i></span>
                    <span class="text-nowrap"><p class="text-white" style="font-size:22px;"><b>{{$row6->cat}}</b></p><span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                        <i class="ni ni-active-40"></i>
                      </div>
                    </div>
                  </div>
                  </div>
                  <div style="background-color:#f5f5f5">
                  <p class="mt-3 mb-0 text-sm">
                  <a href="{{ url('sub_cat_user',['ids'=>$row6->id])}}">
                    <span class="text-nowrap text-black ml-2 px-4" >View Details</span>
                    <span class="text-success"><i class="fa fa-arrow-right text-primary"></i></span>
                  </a>
                </p>
                <br>
                  </div>
              </div>
            @endforeach     
            </div>
            @endif
            @if($BEINSPORTS!=null)
            <div class="col-xl-4 col-md-6">
            @foreach($use8 as $row8)
              <div class="card card-stats">
                <!-- Card body -->
                <div class="card-body" style="background-color:#e0b0ff">
                  <div class="row">
                    <div class="col">
                        
                    <span class="text-success"><i class="fa fa-futbol-o fa-5x" style="float:left;color:white"></i></span>
                    <span class="text-nowrap"><h3 class="text-white">{{$row8->cat}}</h3></span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                        <i class="ni ni-active-40"></i>
                      </div>
                    </div>
                  </div>
                  </div>
                  <div style="background-color:#f5f5f5">
                  <p class="mt-3 mb-0 text-sm">
                  <a href="{{ url('sub_cat_user',['ids'=>$row8->id])}}">
                    <span class="text-nowrap text-black ml-2 px-4" >View Details</span>
                    <span class="text-success"><i class="fa fa-arrow-right text-primary "></i></span>
                    </a>
                </p>
                <br>
                  </div>
              </div>
            @endforeach  
            </div>
            @endif
            @if($NETFLIXACCOUNTS!=null) 
            <div class="col-xl-4 col-md-6">
            @foreach($use9 as $row9)
              <div class="card card-stats">
                <!-- Card body -->
                <div class="card-body" style="background-color:red">
                  <div class="row">
                    <div class="col">
                        
                    <span class="text-success"><i class="fa fa-mobile fa-5x" style="float:left;color:white"></i></span>
                    <span class="text-nowrap"><b><h2  class="text-white"><pre class="text-white" style="font-size:25px;">{{$row9->cat}}</pre></h2></b></span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                        <i class="ni ni-active-40"></i>
                      </div>
                    </div>
                  </div>
                  </div>
                  <div style="background-color:#f5f5f5">
                  <p class="mt-3 mb-0 text-sm">
                  <a href="{{ url('sub_cat_user',['ids'=>$row9->id])}}">
                    <span class="text-nowrap text-black ml-2 px-4" >View Details</span>
                    <span class="text-success"><i class="fa fa-arrow-right text-primary arrow"></i></span>
                    </a>
                </p>
                <br>
                  </div>
              </div>
            @endforeach  
            </div>
            @endif
            @if($MOBILEINTERNET!=null)
            <div class="col-xl-4 col-md-4">
            @foreach($use7 as $row7)
              <div class="card card-stats">
                <!-- Card body -->
                <div class="card-body" style="background-color:orange">
                  <div class="row">
                    <div class="col">
                        
                    <span class="text-success"><i class="fa fa-wifi fa-5x" style="float:left;color:white"></i></span>
                    <span class="text-nowrap"><h3 class="text-white">{{$row7->cat}}</h3></span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                        <i class="ni ni-active-40"></i>
                      </div>
                    </div>
                  </div>
                  </div>
                  <div style="background-color:#f5f5f5">
                  <p class="mt-3 mb-0 text-sm">
                  <a href="{{ url('sub_cat_user',['ids'=>$row7->id])}}">
                    <span class="text-nowrap text-black ml-2 px-4" >View Details</span>
                    <span class="text-success"><i class="fa fa-arrow-right text-primary "></i></span>
                    </a>
                </p>
                <br>
                  </div>
              </div>
            @endforeach  
            </div>
            @endif
            </div>

            </div>
          </div>
        </div>
      </div>
    </div>				                 
																
                    <!-- ============================================================== -->
                    <!-- end pageheader  -->
                    <!-- ============================================================== -->
                   
                </div>
            </div>

            
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
     
            <!-- ============================================================== -->
            <!-- end footer -->
            <!-- ============================================================== -->
        </div>

@endsection