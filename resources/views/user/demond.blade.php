@extends('user/user_index')


@section('content')
<div class="dashboard-wrapper">
            <div class="container-fluid  dashboard-content">
                <!-- ============================================================== -->
                <!-- pageheader -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="page-header">
                            <p class="pageheader-title heading-size">Demond Sold</p>
                            <p class="pageheader-text">Proin placerat ante duiullam scelerisque a velit ac porta, fusce sit amet vestibulum mi. Morbi lobortis pulvinar quam.</p>
                            <div class="page-breadcrumb">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><i class="fa fa-chart-pie"></i><a href=" {{url('dashboard')}}" class="breadcrumb-link"> Dashboard</a></li>
                                        <li class="breadcrumb-item"><i class="fa fa-table"></i><a href="{{url('sold')}}" class="breadcrumb-link"> Last 20 Transfers</a></li>
                                        <li class="breadcrumb-item"><i class="fa fa-table"></i><a  class="breadcrumb-link"> Demond Sold</a></li>
                                    </ol>
                                  
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            
                @if(Session::has('success'))
                                    <div class="alert alert-success">
                                    {{ Session::get('success') }}
                                    @php
                                    Session::forget('success');
                                    @endphp
                                    </div>
                                @endif
                <form class="needs-validation form-group" method="post" action="{{ url('demand_solds') }}" role="form">
                @csrf()
                <p class="pageheader-title heading-size">Send Demond</p>
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 ">
                    <div class="input-group">
                        <input type="hidden" class="form-control" id="validationCustomUsername" name="name" value="<?php echo "$data" ?>"  >
                        <input type="hidden" class="form-control" id="validationCustomUsername" name="id" value="<?php echo "$data1" ?>"  >
                        <input type="hidden" class="form-control" id="validationCustomUsername" name="addby" value="<?php echo "$data2" ?>"  >


                        <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroupPrepend">$</span>
                        </div>
                        <input type="number" class="form-control" id="validationCustomUsername" name="demand">
                   </div>
                </div>
                <input type="submit" class="btn btn-primary mt-3 ml-3" value=".::.Send.::.">                   
                </form><br>
                <div class="row">
                    <!-- ============================================================== -->
                    <!-- basic table  -->
                    <!-- ============================================================== -->
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered first">
                                        <thead>
                                            <tr>
                                                <th>Demond</th>
                                                <th>Date</th>
                                                <th>State</th>
                                            </tr>
                                        </thead>
                                             
                                        <tbody>
                                                
                                                @foreach($data4 as $row1)
                                                <tr>
                                                  <td>{{$row1->solds}}</td>
                                                  <td>{{$row1->created_at}}</td>
                                                  <td> 
                                                  @if (($row1->state)==null)
                                                  pennding
                                                  @else
                                                  approve
                                    
                                                  @endif</td>
                                                  </tr>
                                                 @endforeach 
                                                
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
</div>
</div>
@endsection