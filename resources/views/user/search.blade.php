@extends('user/user_index')

@section('content')
<script>
    // Data Picker Initialization
$('.datepicker').datepicker();
</script>
<div class="dashboard-wrapper">
            <div class="container-fluid  dashboard-content">
                <!-- ============================================================== -->
                <!-- pageheader -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="page-header">
                            <p class="pageheader-title heading-size">Last 50 Codes</p>
                            <p class="pageheader-text">Proin placerat ante duiullam scelerisque a velit ac porta, fusce sit amet vestibulum mi. Morbi lobortis pulvinar quam.</p>
                            <div class="page-breadcrumb">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><i class="fa fa-chart-pie"></i><a href=" {{url('dashboard')}}" class="breadcrumb-link"> Dashboard</a></li>
                                        <li class="breadcrumb-item"><i class="fa fa-table"></i><a href="{{url('code')}}" class="breadcrumb-link"> Last 50 code</a></li>
                                        <li class="breadcrumb-item"><i class="fa fa-table"></i><a href="{{url('search')}}" class="breadcrumb-link"> Search</a></li>
                                    </ol>
                                  
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <form>
                    <div class="row">
                <div class="col-xl-3 col-lg-4 col-md-4 col-sm-4 col-3 ">
                    <div class="input-group">
                    <label class="custom-control custom-radio custom-control-inline">
                    <input type="radio" name="radio-inline" class="custom-control-input" ><span class="custom-control-label mr-2">From</span>
                    <input type="date" class="form-control" id="validationCustomUsername" >
                     </div>
                     
                    
                    </div>
                    <div class="col-xl-3 col-lg-4 col-md-4 col-sm-4 col-3 ">
                    <div class="input-group">
                        <label class="mt-1 mr-3">To:</label>
                    <input type="date" class="form-control" id="validationCustomUsername" >
                     </div>
                    </div>
                    </div>
                    <div class="col-xl-6 col-lg-4 col-md-4 col-sm-4 col-6 mt-2 ">
                    <div class="form-group">
                    
                     <select class="form-control" id="input-select">
                        <option>All Codes</option>
                        </select>
                </div>
                    </div>
                    <div>
                    <label class="custom-control custom-radio custom-control-inline">
                        <input type="radio" name="radio-inline" class="custom-control-input" ><span class="custom-control-label">Search by</span>
                         </label>
                    </div>
                    <div class="col-xl-6 col-lg-4 col-md-4 col-sm-4 col-6 mt-2 ">
                    <input type="text" class="form-control" placeholder="Search by" id="validationCustomUsername"   >
                    <select class="form-control" id="input-select">
                        <option>Serials</option>
                        <option>Client Name</option>
                        </select>   
                         </div>
                         <div class="form-group ml-4 mt-4"> 
                    <input type="submit" class="btn-primary" value=".:::. Search .:::.">                       
                    </div>
                </form>
</div>
</div>
@endsection