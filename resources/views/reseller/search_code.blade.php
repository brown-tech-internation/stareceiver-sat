@extends('..layouts/reseller')

@section('content')
<script>
    // Data Picker Initialization
$('.datepicker').datepicker();
</script>
<div class="dashboard-wrapper">
            <div class="container-fluid  dashboard-content">
                <!-- ============================================================== -->
                <!-- pageheader -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="page-header">
                            <p class="pageheader-title heading-size">Last 50 Codes</p>
                            <p class="pageheader-text">Proin placerat ante duiullam scelerisque a velit ac porta, fusce sit amet vestibulum mi. Morbi lobortis pulvinar quam.</p>
                            <div class="page-breadcrumb">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><i class="fa fa-chart-pie"></i><a href=" {{url('re_index')}}" class="breadcrumb-link"> Dashboard</a></li>
                                        <li class="breadcrumb-item"><i class="fa fa-table"></i><a href="{{url('code')}}" class="breadcrumb-link"> Last 50 code</a></li>
                                        <li class="breadcrumb-item"><i class="fa fa-table"></i><a href="{{url('search')}}" class="breadcrumb-link"> Search</a></li>
                                    </ol>
                                  
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <form  method="POST" action="{{ url('serch') }}" role="form">
                            @csrf()
                    <div class="row">
                <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-6 ">
                    <div class="input-group">
                    <label class="custom-control custom-radio custom-control-inline">
                    <input type="radio" name="radio-inline" class="custom-control-input" ><span class="custom-control-label mr-2">From</span>
                    <input type="date" class="form-control" name="from" id="validationCustomUsername" >
                     </div>
                     
                    
                    </div>
                    <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-6 ">
                    <div class="input-group">
                        <label class="mt-1 mr-3">To:</label>
                    <input type="date" name="to" class="form-control" value="<?php echo $today?>" id="validationCustomUsername" >
                     </div>
                    </div>
                    </div>
                    <div class="col-xl-6 col-lg-4 col-md-4 col-sm-12 col-12 mt-2 ">
                    <div class="form-group">
                    
                    <select class="form-control" id="input-select" name="ids">
                    <option value="">All Codes</option>
                    @foreach($sub as $sub)
                        <option value="{{$sub->ids}}"> {{$sub->name}}</option>
                    @endforeach    
                    </select>
                </div>
                    </div>
                    <div>
                    <label class="custom-control custom-radio custom-control-inline">
                        <input type="radio" name="radio-inline" class="custom-control-input" ><span class="custom-control-label">Search by</span>
                         </label>
                    </div>
                    <div class="col-xl-6 col-lg-4 col-md-4 col-sm-4 col-6 mt-2 ">
                    <input type="text" class="form-control" placeholder="Search by" id="validationCustomUsername">
                    <select class="form-control" name="" id="input-select">
                        <option value="s">Serials</option>
                        <option value="c">Client Name</option>
                        </select>   
                         </div>
                         <div class="form-group ml-4 mt-4"> 
                    <input type="submit" class="btn-primary" value=".:::. Search .:::.">                       
                    </div>
                </form>
</div>
</div>
@if(Session::has('no-results'))
    <div class="container">
    <div class="alert alert-danger"><span>{{ Session::get('no-results') }}</span></div></div>
@else
@endif
@endsection