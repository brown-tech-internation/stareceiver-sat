@extends('..layouts/reseller')
@section('change_price')active @endsection

@section('content')

<div class="dashboard-wrapper">
            <div class="container-fluid  dashboard-content">
            <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="page-header">
                            <p class="pageheader-title heading-size">Change Prices</p>
                            <p class="pageheader-text">Proin placerat ante duiullam scelerisque a velit ac porta, fusce sit amet vestibulum mi. Morbi lobortis pulvinar quam.</p>
                            <div class="page-breadcrumb bg-light">
                                <nav aria-label="breadcrumb ">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><i class="fa fa-chart-pie"></i><a href="{{url('re_index')}}" class="breadcrumb-link"> Dashboard</a></li>
                                        <li class="breadcrumb-item"><i class="fa fa-table"></i><a href="{{url('change_price')}}" class="breadcrumb-link"> change-prices</a></li>
                                        <!-- <li class="breadcrumb-item"><i class="fa fa-table"></i><a href="/wp-admin/add-new-package" class="breadcrumb-link"> </a></li> -->
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <!-- ============================================================== -->
                    <!-- basic table  -->
                    <!-- ============================================================== -->
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="card">
                            
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-striped ">
                                        <thead>
                                            <tr>
                                                <th>Reseller</th>
                                            </tr>
                                        </thead>
                                        <tbody>
									
									  <tr>
											<td><a href="{{url('change_code')}}">SDS</a></td>
										</tr>
									  <tr>
											<td><a href="changecode.php?type=forsds">Forever SDS</a></td>
										</tr>
									  <tr>
											<td><a href="changecode.php?type=gshare">Gshare</a></td>
										</tr>
									  <tr>
											<td><a href="changecode.php?type=funcam">Funcam Ser</a></td>
										</tr>
									  <tr>
											<td><a href="changecode.php?type=forint">Forever Internet</a></td>
										</tr>
									  <tr>
											<td><a href="changecode.php?type=vanilla">Vanilla</a></td>
										</tr>
									  <tr>
											<td><a href="changecode.php?type=star">StarCam Pro</a></td>
										</tr>
									  <tr>
											<td><a href="changecode.php?type=nashare">NaShare</a></td>
										</tr>
									  <tr>
											<td><a href="changecode.php?type=giptv">G-IPTV</a></td>
										</tr>
									  <tr>
											<td><a href="changecode.php?type=funip">Funcam IPTV</a></td>
										</tr>
									  <tr>
											<td><a href="changecode.php?type=forip">Forever IPTV</a></td>
										</tr>
									  <tr>
											<td><a href="changecode.php?type=volka">Volka 1 Year</a></td>
										</tr>
									  <tr>
											<td><a href="changecode.php?type=volka6">Volka 6 Months</a></td>
										</tr>
									  <tr>
											<td><a href="changecode.php?type=atlaspro">Atlas Pro</a></td>
										</tr>
									  <tr>
											<td><a href="changecode.php?type=gogo">GoGo</a></td>
										</tr>
									  <tr>
											<td><a href="changecode.php?type=pop">POP IPTV</a></td>
										</tr>
									  <tr>
											<td><a href="changecode.php?type=neo">Neotv IPTV</a></td>
										</tr>
									  <tr>
											<td><a href="changecode.php?type=iron">IRON IPTV</a></td>
										</tr>
									  <tr>
											<td><a href="changecode.php?type=smartp">smart+ IPTV</a></td>
										</tr>
									  <tr>
											<td><a href="changecode.php?type=dark">Dark IPTV</a></td>
										</tr>
									  <tr>
											<td><a href="changecode.php?type=dragonip">Dragon IPTV</a></td>
										</tr>
									  <tr>
											<td><a href="changecode.php?type=HD200">HD200 1 Year</a></td>
										</tr>
									  <tr>
											<td><a href="changecode.php?type=HD2003">HD200 3 Months</a></td>
										</tr>
									  <tr>
											<td><a href="changecode.php?type=test">Test IPTV</a></td>
										</tr>
									  <tr>
											<td><a href="changecode.php?type=idoom50">idoom 500</a></td>
										</tr>
									  <tr>
											<td><a href="changecode.php?type=idoom100">idoom 1000</a></td>
										</tr>
									  <tr>
											<td><a href="changecode.php?type=4g100">4G 1000</a></td>
										</tr>
									  <tr>
											<td><a href="changecode.php?type=flexy100">Flexy 100 DA</a></td>
										</tr>
									  <tr>
											<td><a href="changecode.php?type=storm100">Storm 100 DA</a></td>
										</tr>
									  <tr>
											<td><a href="changecode.php?type=arselli100">Arselli 100 DA</a></td>
										</tr>
									  <tr>
											<td><a href="changecode.php?type=playstor5">PlayStor 5$</a></td>
										</tr>
									  <tr>
											<td><a href="changecode.php?type=playstor10">PlayStor 10$</a></td>
										</tr>
									  <tr>
											<td><a href="changecode.php?type=bein1">TOP Sport 1 Month</a></td>
										</tr>
									  <tr>
											<td><a href="changecode.php?type=bein3">TOP Sport 3 Months</a></td>
										</tr>
									  <tr>
											<td><a href="changecode.php?type=bein6">TOP Sport 6 Months</a></td>
										</tr>
									  <tr>
											<td><a href="changecode.php?type=bein12">TOP Sport 1 Year</a></td>
										</tr>								
                                </tbody>
                                       
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- ============================================================== -->
                    <!-- end basic table  -->
                    <!-- ============================================================== -->
                </div>
</div>
</div>
@endsection