@extends('..layouts/reseller')

@section('content')

<div class="dashboard-wrapper">
    <div class="container-fluid  dashboard-content">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-header">
                    <p class="pageheader-title heading-size">Change Password</p>
                    <p class="pageheader-text">Proin placerat ante duiullam scelerisque a velit ac porta, fusce sit amet vestibulum mi. Morbi lobortis pulvinar quam.</p>
                    <div class="page-breadcrumb bg-light">
                        <nav aria-label="breadcrumb ">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><i class="fa fa-chart-pie"></i><a href="{{url('re_index')}}" class="breadcrumb-link"> Dashboard</a></li>
                                <li class="breadcrumb-item"><i class="fa fa-table"></i><a  class="breadcrumb-link">Setting</a></li>
                                
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        @if(Session::has('success'))
                                    <div class="alert alert-success">
                                    {{ Session::get('success') }}
                                    @php
                                    Session::forget('success');
                                    @endphp
                                    </div>
                                @endif
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif
        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 ">
            <form class="needs-validation form-group" method="post" action="{{ url('password_update') }}" role="form">
                @csrf()
                 <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                        <label>Old password</label><br>
                        <div class="input-group">
                            
                            <input type="password" class="form-control" id="validationCustomUsername" name="old_password"  value="{{ old('old password') }}" placeholder="old password" aria-describedby="inputGroupPrepend" >
                        </div>
                    </div><br>
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                        <br><label>New password</label><br>
                        <div class="input-group">
                            
                            <input type="password" class="form-control" id="validationCustomUsername" name="password"  value="{{ old('new password') }}" placeholder="new password" aria-describedby="inputGroupPrepend" >
                        </div>
                    </div>
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 "><br>
                        <button class="btn btn-primary" type="submit">update</button>
                    </div>
                </div>
            </form>
            <h1>Telegram Setting</h1><br>
            <h3>To Add Your Telegram Number or to change it<br>
            Send this code<span style="color:red;"> 16002322 </span>to<span style="color:blue;"> @activecodebot</span></h3>
        </div>  

        
    </div>
</div>
@endsection