@extends('..layouts/reseller')
@section('charging')active @endsection

@section('content')

<div class="dashboard-wrapper">
            <div class="container-fluid  dashboard-content">
            <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="page-header">
                            <p class="pageheader-title heading-size">Charging system</p>
                            <p class="pageheader-text">Proin placerat ante duiullam scelerisque a velit ac porta, fusce sit amet vestibulum mi. Morbi lobortis pulvinar quam.</p>
                            <div class="page-breadcrumb bg-light">
                                <nav aria-label="breadcrumb ">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><i class="fa fa-chart-pie"></i><a href="{{url('re_index')}}" class="breadcrumb-link"> Dashboard</a></li>
                                        <li class="breadcrumb-item"><i class="fa fa-table"></i><a href="{{url('charging')}}" class="breadcrumb-link"> Charging</a></li>
                                        <!-- <li class="breadcrumb-item active" aria-current="page">Data Tables</li> -->
                                    </ol>
                                  
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                    <div class="row">
                        <!-- ============================================================== -->
                        <!-- validation form -->
                        <!-- ============================================================== -->
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6">
                            <div class="card">
                                
                                <div class="card-body">
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif

                                @if(Session::has('success'))
                                    <div class="alert alert-success">
                                    {{ Session::get('success') }}
                                    @php
                                    Session::forget('success');
                                    @endphp
                                    </div>
                                @endif
                                    <form class="needs-validation" novalidate  method="POST" action="{{ url('add_charging') }}" role="form">
                                    @csrf()
 
                                        <div class="row">
                                            
                                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                              
                                                <div class="input-group">
                                                    <input type="text" class="form-control" name="serial_no" id="validationCustomUsername" placeholder="S/N" aria-describedby="inputGroupPrepend" required>
                                                    <div class="invalid-feedback">
                                                        Please choose a S/N.
                                                    </div>
                                                
                                            </div></div>
                                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 "><br>
                                                
                                                <input type="number" class="form-control" name="card_no" id="validationCustom01" placeholder="Card No"  required>
                                                <div class="valid-feedback">
                                                    Looks good!
                                                </div>
                                            </div>
                                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 "><br>
                                            <div class="form-group">
                                                <select class="form-control" name="cat" id="input-select">
                                                    <option>Funcam ser,IPTV</option>
                                                    <option>Gshare,SDS,GIPTV</option>
                                                    <option>Forever ser,IPTV</option>
                                                </select>
                                            </div>
                                            </div>
                                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">                                                <div class="form-group">
                                                    <select class="form-control" name="query" id="input-select">
                                                        <option>query</option>
                                                        <option>charge</option>
                                                        <option>refresh sks</option>
                                                    </select>
                                                </div>
                                                </div>
                                        </div>
                                        
                                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                                <button class="btn btn-primary" type="submit">.::. Send .::.</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- ============================================================== -->
                        <!-- end validation form -->
                        <!-- ============================================================== -->
                    </div>
                    </div>
               <!-- ============================================================== -->
        
                    @endsection