@extends('..layouts/reseller')
@section('all_resellers')active @endsection

@section('content')

<div class="dashboard-wrapper">
            <div class="container-fluid  dashboard-content">
                <!-- ============================================================== -->
                <!-- pageheader -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="page-   1header">
                            <p class="pageheader-title heading-size">Your Resellers</p>
                            <p class="pageheader-text">Proin placerat ante duiullam scelerisque a velit ac porta, fusce sit amet vestibulum mi. Morbi lobortis pulvinar quam.</p>
                            <div class="page-breadcrumb bg-light">
                                <nav aria-label="breadcrumb ">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><i class="fa fa-chart-pie"></i><a href="{{url('re_index')}}" class="breadcrumb-link"> Dashboard</a></li>
                                        <li class="breadcrumb-item"><i class="fa fa-table"></i><a href="{{url('all_resellers')}}" class="breadcrumb-link"> Your Resellers</a></li>
                                        <!-- <li class="breadcrumb-item active" aria-current="page">Data Tables</li> -->
                                    </ol>
                                  
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- end pageheader -->
                <!-- ============================================================== -->
                <div class="row">
                    <!-- ============================================================== -->
                    <!-- basic table  -->
                    <!-- ============================================================== -->
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12"><br>
                        <div class="card">
                            
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered first">
                                        <thead>
                                            <tr>
                                                <th>Reseller</th>
                                                <th>Password</th>
                                                <th>Gros</th>
                                                <th>Sold</th>
                                                <th>2FA</th>
                                                <th>Formate 2FA</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($users as $rows)
                                    <tr>
                                    <td><a href="{{url('r_reslller_info'  ,['ids'=>$rows->id])}}">{{$rows->email}}</a></td>
                                    <td>{{$rows->password}}</td>
                                    <td>{{$rows->addby}}</td>
                                    <td>{{$rows->sold}}</td>
                                    <td></td>
                                    <td></td>
                                    </tr>
                                        @endforeach
								
									</tbody>
                                       
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- ============================================================== -->
                    <!-- end basic table  -->
                    <!-- ============================================================== -->
                </div>
               <!-- ============================================================== -->
        </div>


@endsection