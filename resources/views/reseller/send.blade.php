@extends('..layouts/reseller')
@section('send')active @endsection

@section('content')

<div class="dashboard-wrapper">
            <div class="container-fluid  dashboard-content">
                <!-- ============================================================== -->
                <!-- pageheader -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="page-header">
                            <p class="pageheader-title heading-size">Sold Not Paied</p>
                            <p class="pageheader-text">Proin placerat ante duiullam scelerisque a velit ac porta, fusce sit amet vestibulum mi. Morbi lobortis pulvinar quam.</p>
                            <div class="page-breadcrumb bg-light">
                                <nav aria-label="breadcrumb ">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><i class="fa fa-chart-pie"></i><a href="{{url('re_index')}}" class="breadcrumb-link"> Dashboard</a></li>
                                        <li class="breadcrumb-item"><i class="fa fa-table"></i><a href="{{url('send')}}" class="breadcrumb-link"> Sold Not Paied</a></li>
                                        <li class="breadcrumb-item"><i class="fa fa-table"></i><a href="{{url('send')}}" class="breadcrumb-link"> Demond Sold From Res.</a></li>
                                        
                                        <!-- <li class="breadcrumb-item active" aria-current="page">Data Tables</li> -->
                                    </ol>
                                  
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- end pageheader -->
                <!-- ============================================================== -->
                <div class="row">
                    <!-- ============================================================== -->
                    <!-- basic table  -->
                    <!-- ============================================================== -->
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="card">
                        @if(Session::has('success'))
                                    <div class="alert alert-success">
                                    {{ Session::get('success') }}
                                    @php
                                    Session::forget('success');
                                    @endphp
                                    </div>
                                @endif
                            
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered first">
                                        <thead>
                                            <tr>
                                                <th>User</th>
                                                <th>sold</th>
                                                <th>approve</th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
								
									@foreach($dataes as $row)
									<tr class="active">
                                    <td>{{$row->email}}</td>
                                    <td>{{$row->soldss}}</td>
                                    <td>
                                    <form class="needs-validation form-group" method="post" action="{{ url('approves' ,['id'=>$row->id,'ids'=>$row->addby,'idd'=>$row->ids]) }}" role="form">
                                    @csrf()
                                    <input type="hidden" class="form-control" id="validationCustomUsername" name="sold"  value="{{$row->soldss}}" placeholder="Username" aria-describedby="inputGroupPrepend" >
                                    <button class="btn btn-primary" type="submit">Approve</button>
                                    </form>             
                
                        </td>
                                    </tr>
                                    @endforeach
									
									</tbody>
                                       
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- ============================================================== -->
                    <!-- end basic table  -->
                    <!-- ============================================================== -->
                </div>
               <!-- ============================================================== -->
        </div>


@endsection