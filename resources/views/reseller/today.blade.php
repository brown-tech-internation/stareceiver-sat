@extends('..layouts/reseller')
@section('today')active @endsection

@section('content')


<div class="dashboard-wrapper">
            <div class="container-fluid dashboard-content">
                <!-- ============================================================== -->
                <!-- pageheader -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="page-1header">
                            <p class="pageheader-title heading-size">Selling Today</p>
                            <p class="pageheader-text">Proin placerat ante duiullam scelerisque a velit ac porta, fusce sit amet vestibulum mi. Morbi lobortis pulvinar quam.</p>
                            <div class="page-breadcrumb bg-light">
                                <nav aria-label="breadcrumb ">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><i class="fa fa-chart-pie"></i><a href="{{url('re_index')}}" class="breadcrumb-link"> Dashboard</a></li>
                                        <li class="breadcrumb-item"><i class="fa fa-table"></i><a href="{{url('today')}}" class="breadcrumb-link"> Selling</a></li>
                                        <li class="breadcrumb-item"><i class="fa fa-table"></i><a href="{{url('today')}}" class="breadcrumb-link"> Today</a></li>

                                        <!-- <li class="breadcrumb-item active" aria-current="page">Data Tables</li> -->
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- end pageheader -->
                <!-- ============================================================== -->
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12"><br>
                        <div class="card">
                            
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered first">
                                        <thead>
                                        <tr>
                                       @php
                                       $conn= mysqli_connect('localhost','root','','star');
                                       if(!$conn) {
                                       die("Connection Failed: ". mysqli_connect_error());
                                       }
                                       $sql="SELECT name FROM sub_cat where id='1'";
                                        $result= $conn->query($sql);
                                        if($result->num_rows > 0)  
                                           {
                                         while($row= $result->fetch_assoc())
                                         {
                                               echo "<th>".$row['name']."</th>";
                                               
                                               
       
                                               
                                               }

                                        }
                                    echo "</tr>";

                                    echo "<tr>";


                                    $sql="SELECT ids FROM sub_cat where id='1'";
                                        $result= $conn->query($sql);
                                        if($result->num_rows > 0)  
                                           {
                                         while($row= $result->fetch_assoc())
                                         {
                                          $ids=  $row['ids'];
                                          $date= date("Y-m-d");
                                            $sql1="SELECT code FROM user_code_pur where ids='$ids' and purchaser_id='$id' and creade='$date'";
                                        $result1= $conn->query($sql1);
                                        $counter=0;
                                        if($result1->num_rows > 0)  
                                           {
                                         while($row1= $result1->fetch_assoc())
                                         {
                                             ++$counter;

                                         }
                                         } 
                                               
                                               
                                               echo "<th>".$counter."</th>";
                                               
                                               
       
                                               
                                               }

                                        }



                                    
                                    
                                    
                                    
                                    
                                      echo "</tr>";

                                       @endphp

                                        </thead>
                                        <tbody>
                                        <tr></tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered first">
                                        <thead>
                                       
                                        <tr>
                                        @php
                                           
                                        $sql="SELECT name FROM sub_cat where id='2'";
                                        $result= $conn->query($sql);
                                        if($result->num_rows > 0)  
                                           {
                                            while($row= $result->fetch_assoc())
                                                {
                                                    echo "<th>".$row['name']."</th>";
                                                }
                                            }
                                            echo "</tr>";
                                            echo "<tr>";


                                        $sql="SELECT ids FROM sub_cat where id='2'";
                                            $result= $conn->query($sql);
                                                if($result->num_rows > 0)  
                                                    {
                                                        while($row= $result->fetch_assoc())
                                                        {
                                                            $ids=  $row['ids'];
                                                            $date= date("Y-m-d");
                                                            $sql1="SELECT code FROM user_code_pur where ids='$ids'  and purchaser_id='$id' and creade='$date'";
                                                            $result1= $conn->query($sql1);
                                                            $counter=0;
                                                            if($result1->num_rows > 0)  
                                                            {
                                                            while($row1= $result1->fetch_assoc())
                                                            {
                                                                ++$counter;
                                                            }
                                                            } 
                                                            echo "<th>".$counter."</th>";
                                                        }
                                                    }
                                                    echo "</tr>";
                                        @endphp
                                        </thead>
                                        <tbody>
                                        <tr></tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered first">
                                        <thead>
                                       
                                        <tr>
                                        @php
                                           
                                        $sql="SELECT name FROM sub_cat where id='3'";
                                        $result= $conn->query($sql);
                                        if($result->num_rows > 0)  
                                           {
                                            while($row= $result->fetch_assoc())
                                                {
                                                    echo "<th>".$row['name']."</th>";
                                                }
                                            }
                                            echo "</tr>";
                                            echo "<tr>";


                                        $sql="SELECT ids FROM sub_cat where id='3'";
                                            $result= $conn->query($sql);
                                                if($result->num_rows > 0)  
                                                    {
                                                        while($row= $result->fetch_assoc())
                                                        {
                                                            $ids=  $row['ids'];
                                                            $date= date("Y-m-d");
                                                            $sql1="SELECT code FROM user_code_pur where ids='$ids'  and purchaser_id='$id' and creade='$date'";
                                                            $result1= $conn->query($sql1);
                                                            $counter=0;
                                                            if($result1->num_rows > 0)  
                                                            {
                                                            while($row1= $result1->fetch_assoc())
                                                            {
                                                                ++$counter;
                                                            }
                                                            } 
                                                            echo "<th>".$counter."</th>";
                                                        }
                                                    }
                                                    echo "</tr>";
                                        @endphp
                                       
                                        </thead>
                                        <tbody>
                                        <tr></tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered first">
                                        <thead>
                                        <tr>
                                        @php
                                           
                                        $sql="SELECT name FROM sub_cat where id='4'";
                                        $result= $conn->query($sql);
                                        if($result->num_rows > 0)  
                                           {
                                            while($row= $result->fetch_assoc())
                                                {
                                                    echo "<th>".$row['name']."</th>";
                                                }
                                            }
                                            echo "</tr>";
                                            echo "<tr>";


                                        $sql="SELECT ids FROM sub_cat where id='4'";
                                            $result= $conn->query($sql);
                                                if($result->num_rows > 0)  
                                                    {
                                                        while($row= $result->fetch_assoc())
                                                        {
                                                            $ids=  $row['ids'];
                                                            $date= date("Y-m-d");
                                                            $sql1="SELECT code FROM user_code_pur where ids='$ids'  and purchaser_id='$id' and creade='$date'";
                                                            $result1= $conn->query($sql1);
                                                            $counter=0;
                                                            if($result1->num_rows > 0)  
                                                            {
                                                            while($row1= $result1->fetch_assoc())
                                                            {
                                                                ++$counter;
                                                            }
                                                            } 
                                                            echo "<th>".$counter."</th>";
                                                        }
                                                    }
                                                    echo "</tr>";
                                        @endphp
                                       
                                        </thead>
                                        <tbody>
                                        <tr></tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered first">
                                        <thead>
                                        <tr>
                                        @php
                                           
                                        $sql="SELECT name FROM sub_cat where id='5'";
                                        $result= $conn->query($sql);
                                        if($result->num_rows > 0)  
                                           {
                                            while($row= $result->fetch_assoc())
                                                {
                                                    echo "<th>".$row['name']."</th>";
                                                }
                                            }
                                            echo "</tr>";
                                            echo "<tr>";


                                        $sql="SELECT ids FROM sub_cat where id='5'";
                                            $result= $conn->query($sql);
                                                if($result->num_rows > 0)  
                                                    {
                                                        while($row= $result->fetch_assoc())
                                                        {
                                                            $ids=  $row['ids'];
                                                            $date= date("Y-m-d");
                                                            $sql1="SELECT code FROM user_code_pur where ids='$ids' and purchaser_id='$id' and creade='$date'";
                                                            $result1= $conn->query($sql1);
                                                            $counter=0;
                                                            if($result1->num_rows > 0)  
                                                            {
                                                            while($row1= $result1->fetch_assoc())
                                                            {
                                                                ++$counter;
                                                            }
                                                            } 
                                                            echo "<th>".$counter."</th>";
                                                        }
                                                    }
                                                    echo "</tr>";
                                        @endphp
                                        </thead>
                                        <tbody>
                                        <tr></tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered first">
                                        <thead>
                                       
                                        <tr>
                                        @php
                                           
                                        $sql="SELECT name FROM sub_cat where id='6'";
                                        $result= $conn->query($sql);
                                        if($result->num_rows > 0)  
                                           {
                                            while($row= $result->fetch_assoc())
                                                {
                                                    echo "<th>".$row['name']."</th>";
                                                }
                                            }
                                            echo "</tr>";
                                            echo "<tr>";


                                        $sql="SELECT ids FROM sub_cat where id='6'";
                                            $result= $conn->query($sql);
                                                if($result->num_rows > 0)  
                                                    {
                                                        while($row= $result->fetch_assoc())
                                                        {
                                                            $ids=  $row['ids'];
                                                            $date= date("Y-m-d");
                                                            $sql1="SELECT code FROM user_code_pur where ids='$ids' and purchaser_id='$id' and creade='$date'";
                                                            $result1= $conn->query($sql1);
                                                            $counter=0;
                                                            if($result1->num_rows > 0)  
                                                            {
                                                            while($row1= $result1->fetch_assoc())
                                                            {
                                                                ++$counter;
                                                            }
                                                            } 
                                                            echo "<th>".$counter."</th>";
                                                        }
                                                    }
                                                    echo "</tr>";
                                        @endphp
                                        </thead>
                                        <tbody>
                                        <tr></tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered first">
                                        <thead>
                                        <tr>
                                        @php
                                           
                                        $sql="SELECT name FROM sub_cat where id='7'";
                                        $result= $conn->query($sql);
                                        if($result->num_rows > 0)  
                                           {
                                            while($row= $result->fetch_assoc())
                                                {
                                                    echo "<th>".$row['name']."</th>";
                                                }
                                            }
                                            echo "</tr>";
                                            echo "<tr>";


                                        $sql="SELECT ids FROM sub_cat where id='7'";
                                            $result= $conn->query($sql);
                                                if($result->num_rows > 0)  
                                                    {
                                                        while($row= $result->fetch_assoc())
                                                        {
                                                            $ids=  $row['ids'];
                                                            $date= date("Y-m-d");
                                                            $sql1="SELECT code FROM user_code_pur where ids='$ids'  and purchaser_id='$id' and creade='$date'";
                                                            $result1= $conn->query($sql1);
                                                            $counter=0;
                                                            if($result1->num_rows > 0)  
                                                            {
                                                            while($row1= $result1->fetch_assoc())
                                                            {
                                                                ++$counter;
                                                            }
                                                            } 
                                                            echo "<th>".$counter."</th>";
                                                        }
                                                    }
                                                    echo "</tr>";
                                        @endphp
                                        </thead>
                                        <tbody>
                                        <tr></tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered first">
                                        <thead>
                                        <tr>
                                        @php
                                           
                                        $sql="SELECT name FROM sub_cat where id='8'";
                                        $result= $conn->query($sql);
                                        if($result->num_rows > 0)  
                                           {
                                            while($row= $result->fetch_assoc())
                                                {
                                                    echo "<th>".$row['name']."</th>";
                                                }
                                            }
                                            echo "</tr>";
                                            echo "<tr>";


                                        $sql="SELECT ids FROM sub_cat where id='8'";
                                            $result= $conn->query($sql);
                                                if($result->num_rows > 0)  
                                                    {
                                                        while($row= $result->fetch_assoc())
                                                        {
                                                            $ids=  $row['ids'];
                                                            $date= date("Y-m-d");
                                                            $sql1="SELECT code FROM user_code_pur where ids='$ids' and purchaser_id='$id' and creade='$date'";
                                                            $result1= $conn->query($sql1);
                                                            $counter=0;
                                                            if($result1->num_rows > 0)  
                                                            {
                                                            while($row1= $result1->fetch_assoc())
                                                            {
                                                                ++$counter;
                                                            }
                                                            } 
                                                            echo "<th>".$counter."</th>";
                                                        }
                                                    }
                                                    echo "</tr>";
                                        @endphp
                                       
                                        </thead>
                                        <tbody>
                                        <tr></tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    <!-- ============================================================== -->
                    <!-- end basic table  -->
                    <!-- ============================================================== -->
                </div>
               <!-- ============================================================== -->
        </div>


@endsection