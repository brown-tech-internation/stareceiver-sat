@extends('..layouts/reseller')
@section('packages')active @endsection

@section('content')

<div class="dashboard-wrapper">
            <div class="container-fluid  dashboard-content">
            <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="page-header">
                            <p class="pageheader-title heading-size">Your Packages</p>
                            <p class="pageheader-text">Proin placerat ante duiullam scelerisque a velit ac porta, fusce sit amet vestibulum mi. Morbi lobortis pulvinar quam.</p>
                            <div class="page-breadcrumb bg-light">
                                <nav aria-label="breadcrumb ">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><i class="fa fa-chart-pie"></i><a href="{{url('re_index')}}" class="breadcrumb-link"> Dashboard</a></li>
                                        <li class="breadcrumb-item"><i class="fa fa-table"></i><a href="{{url('ressller_pack')}}" class="breadcrumb-link"> Packages</a></li>
                                        <li class="breadcrumb-item"><i class="fa fa-table"></i><a href="{{url('add_package')}}" class="breadcrumb-link"> Add new package</a></li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}(Package Name field)</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if(Session::has('success'))
                                    <div class="alert alert-success">
                                    {{ Session::get('success') }}
                                    @php
                                    Session::forget('success');
                                    @endphp
                                    </div>
                                @endif
                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 ">
                    <form class="needs-validation form-group" novalidate method="POST" action="{{ url('reseller_package') }}" role="form">
                            @csrf()
                        <label>Package Name</label>
                            <div class="input-group">                      
                                <input type="text" class="form-control" name="p_name" id="validationCustomUsername"  aria-describedby="inputGroupPrepend" required>
                                <input type="hidden" class="form-control" name="sess_id" value="@php echo $id @endphp"  aria-describedby="inputGroupPrepend" required>
                                @php $i=0; @endphp
                                    @while($i<=0)
                                   
                                    
                                <input type="hidden" class="form-control" name="p_id" value=" {{$users->get($i)->p_id}}"  aria-describedby="inputGroupPrepend">
                                @php ++$i; @endphp
                                    @endwhile
                                    <div class="invalid-feedback">
                                        Please choose a username.
                                    </div>
            
                            </div>

                       <div class="row">
                    <!-- ============================================================== -->
                    <!-- basic table  -->
                  
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12"><br>
                                <div class="card">
                            
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered ">
                                            <thead>
                                    
                                                <tr>
                                                    <th>Type</th>
										            <th>Price</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($users as $row)
                                                    @if(($row->pk_price)!=null)
                                                <tr>
                                
                                                    <th>{{$row->name}}({{$row->pk_price}})<input type="hidden"  name="ids[]" value="{{$row->ids}}"></th>
                                
                                                    <th><input type="number" name="pri[]" class="form-control"></th>
                               
                                                </tr>
                                                    @endif
                                                @endforeach
                                            </tbody>
                                       
                                            </table>
                                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 "><br>
                                                <button class="btn btn-primary" type="submit">.::. Add .::.</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>   
        </div>
    </div>
    </div>
@endsection