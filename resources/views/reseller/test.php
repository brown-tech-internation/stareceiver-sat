<div class="col-xl-3 col-md-6">
              @foreach($use1 as $row1)           
              <div class="card card-stats">
                <!-- Card body -->
                <div class="card-body" style="background-color:#d9534f">
                  <div class="row">
                    <div class="col">
                     
                    <span class="text-success"><i class="fa fa-desktop fa-5x" style="float:left;color:white"></i></span>
                    <span class="text-nowrap"><h2  class="text-white">{{$row1->cat}}</h2></span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                        <i class="ni ni-active-40"></i>
                      </div>
                    </div>
                  </div>
                  </div>
                  <div style="background-color:#f5f5f5">
                  <p class="mt-3 mb-0 text-sm">
                  <a href="{{ url('sub_cat1',['id'=>$row1->id])}}">
                    <span class="text-nowrap text-black ml-2 px-4" >View Details</span>
                    <span class="text-success"><i class="fa fa-arrow-right text-#d9534f"></i></span>
                  </a>  
                </p>
                <br>
                  </div>
              </div>
              @endforeach 
            </div> 
                   
            <div class="col-xl-3 col-md-6">
              @foreach($use2 as $row2)
              <div class="card card-stats">
                <!-- Card body -->
                <div class="card-body" style="background-color:#5cb85c">
                  <div class="row">
                    <div class="col">
                        
                    <span class="text-success"><i class="fa fa-desktop fa-5x" style="float:left;color:white"></i></span>
                    <span class="text-nowrap"><h2 class="text-white">{{$row2->cat}}</h2></span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                        <i class="ni ni-active-40"></i>
                      </div>
                    </div>
                  </div>
                  </div>
                  <div style="background-color:#f5f5f5">
                  <p class="mt-3 mb-0 text-sm">
                  <a href="{{ url('sub_cat2',['id'=>$row2->id])}}">
                    <span class="text-nowrap text-black ml-2 px-4" >View Details</span>
                    <span class="text-success"><i class="fa fa-arrow-right text-#5cb85c"></i></span>
                    </a>
                </p>
                <br>
                  </div>
              </div>
              @endforeach
            </div>
            <div class="col-xl-3 col-md-6">
               @foreach($use3 as $row3)
              <div class="card card-stats">
                <!-- Card body -->
                <div class="card-body" style="background-color:#00008B">
                  <div class="row">
                    <div class="col">
                        
                    <span class="text-success"><i class="fa fa-desktop fa-5x" style="float:left;color:white"></i></span>
                    <span class="text-nowrap"><h2  class="text-white">{{$row3->cat}}</h2></span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                        <i class="ni ni-active-40"></i>
                      </div>
                    </div>
                  </div>
                  </div>
                  <div style="background-color:#f5f5f5">
                  <p class="mt-3 mb-0 text-sm">
                  <a href="{{ url('sub_cat3',['id'=>$row3->id])}}">
                    <span class="text-nowrap text-black ml-2 px-4" >View Details</span>
                    <span class="text-success"><i class="fa fa-arrow-right text-primary "></i></span>
                    </a>
                </p>
                <br>
                  </div>
              </div>
            @endforeach  
            </div>   
            @foreach($use4 as $row4)  
            <div class="col-xl-3 col-md-6">
              <div class="card card-stats">
                <!-- Card body -->
                <div class="card-body" style="background-color:#f0ad4e">
                  <div class="row">
                    <div class="col">
                        
                    <span class="text-success"><i class="fa fa-desktop fa-5x" style="float:left;color:white"></i></span>
                    <span class="text-nowrap"><h2  class="text-white">{{$row4->cat}}</h2></span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                        <i class="ni ni-active-40"></i>
                      </div>
                    </div>
                  </div>
                  </div>
                  <div style="background-color:#f5f5f5">
                  <p class="mt-3 mb-0 text-sm">
                  <a href="{{ url('sub_cat4',['id'=>$row4->id])}}">
                    <span class="text-nowrap text-black ml-2 px-4" >View Details</span>
                    <span class="text-success"><i class="fa fa-arrow-right text-primary"></i></span>
                    </a>                
                  </p>
                <br>
                  </div>
              </div>
            </div>
            @endforeach
            </div>
            <div class="row">
            @foreach($use5 as $row5)  
            <div class="col-xl-3 col-md-6">
              <div class="card card-stats">
                <!-- Card body -->
                <div class="card-body" style="background-color:#00008B">
                  <div class="row">
                    <div class="col">
                        
                    <span class="text-success"><i class="fa fa-flask fa-5x" style="float:left;color:white"></i></span>
                    <span class="text-nowrap"><h2  class="text-white">{{$row5->cat}}</h2></span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                        <i class="ni ni-active-40"></i>
                      </div>
                    </div>
                  </div>
                  </div>
                  <div style="background-color:#f5f5f5">
                  <p class="mt-3 mb-0 text-sm">
                  <a href="{{ url('sub_cat5',['id'=>$row5->id])}}">
                    <span class="text-nowrap text-black ml-2 px-4" >View Details</span>
                    <span class="text-success"><i class="fa fa-arrow-right text-primary"></i></span>
                    </a>
                </p>
                <br>
                  </div>
              </div>
              @endforeach   
            </div>
            <div class="col-xl-3 col-md-6">
            @foreach($use6 as $row6)  
              <div class="card card-stats">
                <!-- Card body -->
                <div class="card-body" style="background-color:#8A2BE2">
                  <div class="row">
                    <div class="col">
                        
                    <span class="text-success"><i class="fa fa-futbol-o fa-5x" style="float:left;color:white"></i></span>
                    <span class="text-nowrap"><h2  class="text-white">{{$row6->cat}}</h2></span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                        <i class="ni ni-active-40"></i>
                      </div>
                    </div>
                  </div>
                  </div>
                  <div style="background-color:#f5f5f5">
                  <p class="mt-3 mb-0 text-sm">
                  <a href="{{ url('sub_cat6',['id'=>$row6->id])}}">
                    <span class="text-nowrap text-black ml-2 px-4" >View Details</span>
                    <span class="text-success"><i class="fa fa-arrow-right text-primary"></i></span>
                  </a>
                </p>
                <br>
                  </div>
              </div>
            @endforeach     
            </div>
            <div class="col-xl-5 col-md-6">
            @foreach($use7 as $row7)
              <div class="card card-stats">
                <!-- Card body -->
                <div class="card-body" style="background-color:#d9534f">
                  <div class="row">
                    <div class="col">
                        
                    <span class="text-success"><i class="fa fa-desktop fa-5x" style="float:left;color:white"></i></span>
                    <span class="text-nowrap"><h2  class="text-white">{{$row7->cat}}</h2></span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                        <i class="ni ni-active-40"></i>
                      </div>
                    </div>
                  </div>
                  </div>
                  <div style="background-color:#f5f5f5">
                  <p class="mt-3 mb-0 text-sm">
                  <a href="{{ url('sub_cat3',['id'=>$row3->id])}}">
                    <span class="text-nowrap text-black ml-2 px-4" >View Details</span>
                    <span class="text-success"><i class="fa fa-arrow-right text-primary "></i></span>
                    </a>
                </p>
                <br>
                  </div>
              </div>
            @endforeach  
            </div>
            <div class="col-xl-4 col-md-6">
            @foreach($use8 as $row8)
              <div class="card card-stats">
                <!-- Card body -->
                <div class="card-body" style="background-color:#5cb85c">
                  <div class="row">
                    <div class="col">
                        
                    <span class="text-success"><i class="fa fa-desktop fa-5x" style="float:left;color:white"></i></span>
                    <span class="text-nowrap"><h2  class="text-white">{{$row8->cat}}</h2></span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                        <i class="ni ni-active-40"></i>
                      </div>
                    </div>
                  </div>
                  </div>
                  <div style="background-color:#f5f5f5">
                  <p class="mt-3 mb-0 text-sm">
                  <a href="{{ url('sub_cat3',['id'=>$row3->id])}}">
                    <span class="text-nowrap text-black ml-2 px-4" >View Details</span>
                    <span class="text-success"><i class="fa fa-arrow-right text-primary "></i></span>
                    </a>
                </p>
                <br>
                  </div>
              </div>
            @endforeach  
            </div>
            <div class="col-xl-5 col-md-6">
            @foreach($use9 as $row9)
              <div class="card card-stats">
                <!-- Card body -->
                <div class="card-body" style="background-color:#f0ad4e">
                  <div class="row">
                    <div class="col">
                        
                    <span class="text-success"><i class="fa fa-desktop fa-5x" style="float:left;color:white"></i></span>
                    <span class="text-nowrap"><h2  class="text-white">{{$row9->cat}}</h2></span>
                    </div>
                    <div class="col-auto">
                      <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                        <i class="ni ni-active-40"></i>
                      </div>
                    </div>
                  </div>
                  </div>
                  <div style="background-color:#f5f5f5">
                  <p class="mt-3 mb-0 text-sm">
                  <a href="{{ url('sub_cat3',['id'=>$row3->id])}}">
                    <span class="text-nowrap text-black ml-2 px-4" >View Details</span>
                    <span class="text-success"><i class="fa fa-arrow-right text-primary arrow"></i></span>
                    </a>
                </p>
                <br>
                  </div>
              </div>
            @endforeach  
            </div>
                  