@extends('..layouts/reseller')
@section('packages')active @endsection

@section('content')

<div class="dashboard-wrapper">
            <div class="container-fluid  dashboard-content">
            <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="page-header">
                            <p class="pageheader-title heading-size">Your Packages</p>
                            <p class="pageheader-text">Proin placerat ante duiullam scelerisque a velit ac porta, fusce sit amet vestibulum mi. Morbi lobortis pulvinar quam.</p>
                            <div class="page-breadcrumb bg-light">
                                <nav aria-label="breadcrumb ">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><i class="fa fa-chart-pie"></i><a href="{{url('re_index')}}" class="breadcrumb-link"> Dashboard</a></li>
                                        <li class="breadcrumb-item"><i class="fa fa-table"></i><a href="{{url('ressller_pack')}}" class="breadcrumb-link"> Packages</a></li>
                                        <li class="breadcrumb-item"><i class="fa fa-table"></i><a href="{{url('add_package')}}" class="breadcrumb-link"> Add new package</a></li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <!-- ============================================================== -->
                    <!-- basic table  -->
                    <!-- ============================================================== -->
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12"><br>
                        <div class="card">
                            
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered ">
                                        <tbody>
                                    @foreach($pack as $rowe)   
									<tr class="active">
                                       <td><a href="{{ url('see_reseller_pack',['pr_id'=>$rowe->pr_id])}}">{{$rowe->p_name}}</td>
                                      
                                    </tr>
                                    @endforeach
									</tbody>
                                       
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- ============================================================== -->
                    <!-- end basic table  -->
                    <!-- ============================================================== -->
                </div>
</div>
</div>
@endsection