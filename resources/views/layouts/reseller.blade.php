<!doctype html>
<html lang="en">
 
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('admin/assets/vendor/bootstrap/css/bootstrap.min.css')}}">
    <link href="{{asset('admin/assets/vendor/fonts/circular-std/style.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('admin/assets/libs/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('admin/assets/vendor/fonts/fontawesome/css/fontawesome-all.css')}}">
    <link rel="stylesheet" href="{{asset('admin/assets/vendor/charts/chartist-bundle/chartist.css')}}">
    <link rel="stylesheet" href="{{asset('admin/assets/vendor/charts/morris-bundle/morris.css')}}">
    <link rel="stylesheet" href="{{asset('admin/assets/vendor/fonts/material-design-iconic-font/css/materialdesignicons.min.css')}}">
    <link rel="stylesheet" href="{{asset('admin/assets/vendor/charts/c3charts/c3.css')}}">
    <link rel="stylesheet" href="{{asset('admin/assets/vendor/fonts/flag-icon-css/flag-icon.min.css')}}">
    <link rel="stylesheet" href="{{asset('admin/assets/vendor/extra.css')}}">
    <title>STARECEIVER-SAT</title>
</head>

<body>
    <!-- ============================================================== -->
    <!-- main wrapper -->
    <!-- ============================================================== -->
    <div class="dashboard-main-wrapper">
        <!-- ============================================================== -->
        <!-- navbar -->
        <!-- ============================================================== -->
        <div class="dashboard-header">
            <nav class="navbar navbar-expand-lg bg-white fixed-top orset">
            @foreach($datas as $row)
            <a class="navbar-brand sold">Your Sold:</a><a class="navbar-brand sold">{{$row->sold}}</a>
            @endforeach
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse " id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto navbar-right-top">
                      
                       
                        <li class="nav-item dropdown nav-user">
                        
                            <a class="navbar-brand" href="#" id="navbarDropdownMenuLink2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-user mr-2"><?php echo "$data" ?></i></a>
                         
                            <div class="dropdown-menu dropdown-menu-right nav-user-dropdown" aria-labelledby="navbarDropdownMenuLink2">
                            
                                <a class="dropdown-item" href="{{url('statics')}}"><i class="fas fa-user mr-2"></i>Statistics</a>
                                <a class="dropdown-item" href="#"><i class="fa fa-envelope mr-2"></i>Inbox</a>
                                <a class="dropdown-item" href="{{url('re_settings')}}"><i class="fas fa-cog mr-2"></i>Setting</a>
                                <hr />
                                <a class="dropdown-item" href="{{url('admin_logout')}}"><i class="fas fa-power-off mr-2"></i>Logout</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
        <!-- ============================================================== -->
        <!-- end navbar -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- left sidebar -->
        <!-- ============================================================== -->
        <div class="nav-left-sidebar sidebar-dark orset">
            <div class="menu-list">
                <nav class="navbar navbar-expand-lg navbar-light">
                    <a class="d-xl-none d-lg-none" href="#">Dashboard</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNav">
                        <ul class="navbar-nav flex-column">
                          
                            <li class="nav-item ">
                                <a class="nav-link @yield('main')" href="{{url('re_index')}}" ><i class="fa fa-fw fa-chart-pie"></i>Dashboard <span class="badge badge-success">6</span></a>
                                
                            </li>
                            <li class="nav-item">
                                <a class="nav-link @yield('code')" href="{{url('codes')}}" ><i class="fa fa-fw fa-table"></i>Codes</a>
                                
                            </li>
                            <li class="nav-item">
                                <a class="nav-link @yield('receive')" href="#" data-toggle="collapse" aria-expanded="false" data-target="#submenu-3" aria-controls="submenu-3"><i class="fas fa-fw fa-dollar-sign"></i>Sold</a>
                                <div id="submenu-3" class="collapse submenu" style="">
                                    <ul class="nav flex-column">
                                        <li class="nav-item">
                                            <a class="nav-link  @yield('send')"  href="{{url('send')}}">Send</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link @yield('receive')" href="{{url('receive')}}">Receive</a>
                                        </li>
                                        
                                    </ul>
                                </div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link @yield('add_user')" href="{{url('add_user')}}" ><i class="fa fa-fw fa-edit"></i>Add User</a>
                                
                            </li>
                            <li class="nav-item">
                                <a class="nav-link @yield('all_resellers')" href="{{url('all_resellers')}}" ><i class="fa fa-fw fa-table"></i>Reseller</a>
                                
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#" data-toggle="collapse" aria-expanded="false" data-target="#submenu-4" aria-controls="submenu-4"><i class="fas fa-fw fa-dollar-sign"></i>Selling</a>
                                <div id="submenu-4" class="collapse submenu" style="">
                                    <ul class="nav flex-column">
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{url('today')}}">Today</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{url('yesterday')}}">Yesterday</a>
                                        </li>
                                        
                                    </ul>
                                </div>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link @yield('charging')" href="{{url('chargings')}} "><i class="fa fa-fw fa-table"></i>Charging System</a>
                                
                            </li>
                            <li class="nav-item">
                                <a class="nav-link @yield('packages')" href="{{url('ressller_pack')}}" ><i class="fa fa-fw fa-tasks"></i>Packages</a>
                                
                            </li>
                            <li class="nav-item">
                                <a class="nav-link @yield('change_price')" href="{{url('change_prices')}}" ><i class="fa fa-fw fa-table"></i>Change Prices</a>
                                
                            </li>
                            <li class="nav-item">
                                <a class="nav-link @yield('begin_sport')" href="{{url('being_sports')}}" ><i class="fa fa-fw fa-edit"></i>BeIN SPORT</a>
                                
                            </li>
                            <li class="nav-item">
                                <a class="nav-link @yield('prices')" href="{{url('pricess')}}" ><i class="fa fa-fw fa-table"></i>Prices</a>
                                
                            </li>
                        
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end left sidebar -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- wrapper  -->
        <!-- ============================================================== -->
        @yield('content')
        <!-- ============================================================== -->
        <!-- end wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- end main wrapper  -->
    <!-- ============================================================== -->
    <!-- Optional JavaScript -->
    <!-- jquery 3.3.1 -->
    <script src="{{asset('admin/assets/vendor/jquery/jquery-3.3.1.min.js')}}"></script>
    <!-- bootstap bundle js -->
    <script src="{{asset('admin/assets/vendor/bootstrap/js/bootstrap.bundle.js')}}"></script>
    <!-- slimscroll js -->
    <script src="{{asset('admin/assets/vendor/slimscroll/jquery.slimscroll.js')}}"></script>
    <!-- main js -->
    <script src="{{asset('admin/assets/libs/js/main-js.js')}}"></script>
    <!-- chart chartist js -->
    <script src="{{asset('admin/assets/vendor/charts/chartist-bundle/chartist.min.js')}}"></script>
    <!-- sparkline js -->
    <script src="{{asset('admin/assets/vendor/charts/sparkline/jquery.sparkline.js')}}"></script>
    <!-- morris js -->
    <script src="{{asset('admin/assets/vendor/charts/morris-bundle/raphael.min.js')}}"></script>
    <script src="{{asset('admin/assets/vendor/charts/morris-bundle/morris.js')}}"></script>
    <!-- chart c3 js -->
    <script src="{{asset('admin/assets/vendor/charts/c3charts/c3.min.js')}}"></script>
    <script src="{{asset('admin/assets/vendor/charts/c3charts/d3-5.4.0.min.js')}}"></script>
    <script src="{{asset('admin/assets/vendor/charts/c3charts/C3chartjs.js')}}"></script>
    <script src="{{asset('admin/assets/libs/js/dashboard-ecommerce.js')}}"></script>
</body>
 
</html>