<!DOCTYPE html>
<html lang="en">
<head>
	<title>Logins</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="{{asset('login/images_login/icons/favicon.ico')}}"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('login/fonts_login/font-awesome-4.7.0/css/font-awesome.min.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('login/fonts_login/iconic/css/material-design-iconic-font.min.css')}}">
<!--===============================================================================================-->
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('login/css_login/util.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('login/css_login/main.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('admin/assets/vendor/bootstrap/css/bootstrap.min.css')}}">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
</head>
<body>

	<div class="container-login100" style="background-image: url('{{asset('login/images_login/blur.jpg')}}');">
		<div class="wrap-login100 p-l-55 p-r-55 p-t-80 p-b-30">
		@if ($errors->any())
        <div class="alert alert-danger">
            <ul>
	            @foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
                @endforeach
           </ul>
        </div>
        @endif
        @if(\Session::has('fail1'))
            <div class="alert alert-danger alert-dismissible">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			{{\Session::get('fail1')}}</div>
		@endif

	
			<form class="login100-form validate-form" action="{{url('admin_do_login')}}" method="post">
			@csrf()
				<span class="login100-form-title p-b-37">
					STARECEIVER-SAT
				</span>

				<div class="wrap-input100 validate-input m-b-20" data-validate="Enter Username or email">
					<input class="input100" type="text" name="email" placeholder="Enter Email">
					<span class="focus-input100"></span>
					
				</div>

				<div class="wrap-input100 validate-input m-b-25" data-validate = "Enter Password">
					<input class="input100" type="password" name="password" placeholder="Enter password">
					<span class="focus-input100"></span>
	

					
				</div>

				<div class="container-login100-form-btn">
					<button class="login100-form-btn" type="submit" style="background-color: #243F9A;">
						Sign In
					</button><br>
				</div>
				<center><a href="{{url('forget_password')}}" >Forget Password</a></center>
				
			
			</form>

			
		</div>
	</div>
	
	

	<div id="dropDownSelect1"></div>
	
<!--===============================================================================================-->
<!--===============================================================================================-->
<!--===============================================================================================-->
	<script src="js_login/main.js"></script>

</body>
</html>