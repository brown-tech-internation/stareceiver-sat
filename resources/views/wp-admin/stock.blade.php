@extends('wp-admin.aside')

@section('content')



<div class="dashboard-wrapper">
            <div class="container-fluid  dashboard-content">
            <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="page-header">
                            <p class="pageheader-title heading-size">Prices</p>
                            <p class="pageheader-text">Proin placerat ante duiullam scelerisque a velit ac porta, fusce sit amet vestibulum mi. Morbi lobortis pulvinar quam.</p>
                            <div class="page-breadcrumb bg-light">
                                <nav aria-label="breadcrumb ">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><i class="fa fa-chart-pie"></i><a href="{{url('wp-admin')}}" class="breadcrumb-link"> Dashboard</a></li>
                                        <li class="breadcrumb-item"><i class="fa fa-table"></i><a href="{{url('stock')}}" class="breadcrumb-link">Stock</a></li>
                                        <li class="breadcrumb-item"><i class="fa fa-table"></i><a href="{{url('last200')}}" class="breadcrumb-link">Last 200 Codes</a></li>
                                    </ol>
                                </nav>
                            </div>
                        </div>

                        
                    </div>
                </div>


                    <div class="row">
                    <!-- ============================================================== -->
                    <!-- basic table  -->
                    <!-- ============================================================== -->
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered ">
                                    <tbody>
                                    @foreach($datas as $subcat)
                                    <tr class="warning">
                                   
                                        <td><a href="{{url('code_stock',['ids'=>$subcat->ids])}}">{{$subcat->name}}</a></td>
                                  
                                    </tr>
                                    @endforeach
                                   
									 
                            </tbody>
                                    </table>
                                    </div>
                                    </div>
                                    </div>
                                    </div>

                                    </div>

                                    </div>
</div>
@endsection