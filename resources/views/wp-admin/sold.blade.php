@extends('wp-admin.aside')
@section('sold')active @endsection

@section('content')

<div class="dashboard-wrapper">
            <div class="container-fluid  dashboard-content">
                <!-- ============================================================== -->
                <!-- pageheader -->
                <!-- ============================================================== -->
                <div class="row">
                               
                    <div class="container">
  
                    <!-- Button to Open the Modal -->
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
                        Send sold</button>
                        <br>
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" style="margin-top:2%;">
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif

                                @if(Session::has('success'))
                                    <div class="alert alert-success">
                                    {{ Session::get('success') }}
                                    @php
                                    Session::forget('success');
                                    @endphp
                                    </div>
                                @endif
                        </div> 
                       

                    <!-- The Modal -->
                        <div class="modal" id="myModal">
                            <div class="modal-dialog">
                            <div class="modal-content">
      
                                <!-- Modal Header -->
                                <div class="modal-header">
                                    <h4 class="modal-title">Alert</h4>
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>
        
                                <!-- Modal body -->
                                <div class="modal-body">
                                
                                    <form  method="POST" action="{{ url('admin_send_sold') }}" role="form">
                                    @csrf()
                                    	
                                    <div class="form-group">
                                        <label for="sel1">Select User Name:</label>
                                        
                                        <select class="form-control" id="sel1"  name="reselller_name">
                                        <option class="form-control" >Select User Name</option>
                                            @foreach($data as $row)
                                            <option value="{{$row->id}}">{{$row->email}}</option>
                                            @endforeach
                                        </select>
                                        
                                    </div>
                                  
                                        <div class="form-group">
                                            <label for="pwd">sold:</label>
                                            <input type="number" class="form-control"name="sold" placeholder="Sold" id="pwd">
                                        </div>
                                        
                                        <button type="submit" class="btn btn-primary">Send Sold</button>
                                    </form>
                                </div>
        
                                <!-- Modal footer -->
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                </div>
        
                            </div>
                            </div>
                        </div>
                    </div>
                    

                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="page-header">
                            <p class="pageheader-title heading-size">Sold Paied</p>
                            <p class="pageheader-text">Proin placerat ante duiullam scelerisque a velit ac porta, fusce sit amet vestibulum mi. Morbi lobortis pulvinar quam.</p>
                            <div class="page-breadcrumb bg-light">
                                <nav aria-label="breadcrumb ">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><i class="fa fa-chart-pie"></i><a href="{{url('wp-admin')}}" class="breadcrumb-link"> Dashboard</a></li>
                                        <li class="breadcrumb-item"><i class="fa fa-table"></i><a href="{{url('wp-admin/sold')}}" class="breadcrumb-link"> sold</a></li>
                                        <li class="breadcrumb-item"><i class="fa fa-table"></i><a href="{{url('sold_req')}}" class="breadcrumb-link">Sold Not Paied</a></li>

                                        <!-- <li class="breadcrumb-item active" aria-current="page">Data Tables</li> -->
                                    </ol>
                                  
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <!-- ============================================================== -->
                    <!-- basic table  -->
                    <!-- ============================================================== -->
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="card">
                            
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered first">
                                        <thead>
                                            <tr>
                                                <th>Reseller</th>
                                                <th>sold</th>
                                                <th>Date</th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
								
                                    @foreach($data as $row1)
									<tr class="active">
                                       <td><a href="{{ url('sold_his',['id'=>$row1->id])}}">{{$row1->email}}</a></td>
                                       <td>{{$row1->sold}}</td>
                                       <td><?php echo date($row1->created_at);?></td>
                                    </tr>
									@endforeach
									</tbody>
                                       
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- ============================================================== -->
                    <!-- end basic table  -->
                    <!-- ============================================================== -->
                </div>
               <!-- ============================================================== -->
        </div>


@endsection