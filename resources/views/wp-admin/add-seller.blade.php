@extends('wp-admin.aside')
@section('add-seller')active @endsection

@section('content')
<div class="dashboard-wrapper">
            <div class="container-fluid  dashboard-content">
            <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="page-header">
                            <p class="pageheader-title heading-size">Add New Reseller</p>
                            <p class="pageheader-text">Proin placerat ante duiullam scelerisque a velit ac porta, fusce sit amet vestibulum mi. Morbi lobortis pulvinar quam.</p>
                            <div class="page-breadcrumb bg-light">
                                <nav aria-label="breadcrumb ">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><i class="fa fa-chart-pie"></i><a href="wp-admin" class="breadcrumb-link"> Dashboard</a></li>
                                        <li class="breadcrumb-item"><i class="fa fa-table"></i><a href="/wp-admin/add-seller" class="breadcrumb-link"> Add New Reseller</a></li>
                                        <!-- <li class="breadcrumb-item active" aria-current="page">Data Tables</li> -->
                                    </ol>
                                  
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                    <div class="row">
                        <!-- ============================================================== -->
                        <!-- validation form -->
                        <!-- ============================================================== -->
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6">
                            <div class="card">
                               
                            <div class="card-body">
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif

                                @if(Session::has('success'))
                                    <div class="alert alert-success">
                                    {{ Session::get('success') }}
                                    @php
                                    Session::forget('success');
                                    @endphp
                                    </div>
                                @endif

                                    <form class="needs-validation form-group" novalidate method="POST" action="{{ url('reseller_save' ) }}" role="form">
                                    @csrf()
                                        <div class="row">
                                            
                                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                              
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" id="inputGroupPrepend">@</span>
                                                    </div>
                                                    <input type="text" class="form-control" id="validationCustomUsername" name="username"  value="{{ old('username') }}" placeholder="Username" aria-describedby="inputGroupPrepend" required>
                                                    <div class="invalid-feedback">
                                                        Please choose a username.
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 "><br>
                                                
                                                <input type="text" class="form-control" id="validationCustom01" name="password" value="{{ old('password') }}" placeholder="First Password"  required>
                                                <div class="valid-feedback">
                                                    Looks good!
                                                </div>
                                            </div>
                                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 "><br>
                                                
                                                <input type="number" class="form-control" id="validationCustom02" name="mobile" placeholder="Mobile"  required>
                                                <div class="valid-feedback">
                                                    Looks good!
                                                </div>
                                            </div>
                                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                            <br>  
                                            <div class="form-group">
                                                    
                                                    <select class="form-control selectpicker border rounded" id="job-type" data-style="btn-black" name="usertype"  data-width="100%" data-live-search="true" title="Select Job Type">
                                                    <option value="null">select type</option>
                                                    <option value="reseller">reseller</option>
                                                    <option value="reseller">user</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        
                                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 "><br>
                                                <button class="btn btn-primary" type="submit">Add</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- ============================================================== -->
                        <!-- end validation form -->
                        <!-- ============================================================== -->
                    </div>
                    </div>
               <!-- ============================================================== -->
        
                    @endsection