@extends('wp-admin.aside')
@section('packages')active @endsection

@section('content')

<div class="dashboard-wrapper">
            <div class="container-fluid  dashboard-content">
            <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="page-header">
                            <p class="pageheader-title heading-size">Your Packages</p>
                            <p class="pageheader-text">Proin placerat ante duiullam scelerisque a velit ac porta, fusce sit amet vestibulum mi. Morbi lobortis pulvinar quam.</p>
                            <div class="page-breadcrumb bg-light">
                                <nav aria-label="breadcrumb ">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><i class="fa fa-chart-pie"></i><a href="/wp-admin" class="breadcrumb-link"> Dashboard</a></li>
                                        <li class="breadcrumb-item"><i class="fa fa-table"></i><a href="{{url('your_pack')}}" class="breadcrumb-link"> Packages</a></li>

                                        <li class="breadcrumb-item"><i class="fa fa-table"></i><a href="{{url('add_new_packages')}}" class="breadcrumb-link"> Add new package</a></li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <!-- ============================================================== -->
                    <!-- basic table  -->
                    <!-- ============================================================== -->
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12"><br>
                        <div class="card">
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                        @foreach($user as $row)
                        <form class="needs-validation form-group" novalidate method="POST" action="{{ url('pack_update' ,['p_id'=>$row->p_id]) }}" role="form">
                            @csrf()
                        @endforeach    
                            <div class="card-body">
                                <div class="table-responsive">
                                
                                    <table class="table table-striped table-bordered first">
                                        <thead>
                                        <label style="color:black;">Packege Name</label>
                                       
                                        <div class="input-group">
                                            <br>
                                            @php $i=0; @endphp
                                            @while($i < 1)
                                           
                                            
                                            <input type="text" class="form-control" value="{{ $user->get($i)->p_name }}" name="p_name" id="validationCustomUsername" placeholder="Enter Packege Name" aria-describedby="inputGroupPrepend" required>
                                            <div class="invalid-feedback">
                                                    Please choose a code.
                                            </div>
                                            @php $i++; @endphp
                                            @endwhile    
                                        </div><br>
                                        
                                        <tr>
                                            <th style="color:black;">Type</th>
                                            <th style="color:black;">prices<th>
                                        </tr>
                                        @foreach($user as $row)
                                            <tr>
                                            
                                                <th>{{$row->name}}({{$row->price}})<input type="hidden" class="form-control" name="ids[]" value="{{$row->ids}}" id="validationCustomUsername" placeholder="Enter Code" aria-describedby="inputGroupPrepend"><input type="hidden" class="form-control" name="p_id[]" value="{{$row->p_id}}">
                                                </th>
                                                <th> 
                                                    <div class="input-group">
                                                        <input type="number" class="form-control" name="pri[]" value="{{$row->pk_price}}" id="validationCustomUsername" placeholder="0.00" aria-describedby="inputGroupPrepend" required>
                                                        <div class="invalid-feedback">
                                                        Please choose a code.
                                                        </div>
                                                    </div>
                                                </th>
                                            </tr>
                                            @endforeach    
                                        </thead>
                                    </table><br>
                                    <button class="btn btn-primary" type="submit">Update</button>
                                </div>
                                
                            </div>
                        </form> 
                        </div>   
                        </div>
                    </div>
                    <!-- ============================================================== -->
                    <!-- end basic table  -->
                    <!-- ============================================================== -->
                </div>
</div>
</div>
@endsection