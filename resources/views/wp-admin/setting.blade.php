@extends('wp-admin.aside')

@section('content')

<div class="dashboard-wrapper">
    <div class="container-fluid  dashboard-content">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-header">
                    <p class="pageheader-title heading-size">Panel Settings</p>
                    <p class="pageheader-text">Proin placerat ante duiullam scelerisque a velit ac porta, fusce sit amet vestibulum mi. Morbi lobortis pulvinar quam.</p>
                    <div class="page-breadcrumb bg-light">
                        <nav aria-label="breadcrumb ">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><i class="fa fa-chart-pie"></i><a href="{{url('wp-admin')}}" class="breadcrumb-link"> Dashboard</a></li>
                                <li class="breadcrumb-item"><i class="fa fa-table"></i><a  class="breadcrumb-link">PanelSetting</a></li>
                                
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">

        @if(Session::has('success'))
                                    <div class="alert alert-success">
                                    {{ Session::get('success') }}
                                    @php
                                    Session::forget('success');
                                    @endphp
                                    </div>
                                @endif
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif
                                </div>
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="card">
                            <h2>Statistics </h2>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered first">
                                        <thead>
                                            <tr>
                                                <th>Your Codes</th>
                                                <th>Debts</th>
                                                <th>Res.sold</th>
                                                
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
								
                                    
									<tr class="active">
                                       <td></td>
                                       <td></td>
                                       <td></td>
                                    </tr>
									
									</tbody>
                                       
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>                    
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 "><br><br>
            <form class="needs-validation form-group" method="post" action="{{ url('advertisement') }}" role="form">
                @csrf()
                 <div class="row">
                <h2>Advertisement</h2> 
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                        <label>Advertisement</label><br>
                        <div class="input-group">
                          
                        <textarea id="w3review" class="form-control" name="ads" value= rows="4" cols="50">@foreach($add as $add){{$add->ads}}@endforeach
                        </textarea>
                        
                        </div>
                    </div><br>
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                        <br><label>Show</label><br>
                        <div class="input-group">
                            <select class="form-control selectpicker border rounded" id="job-type" data-style="btn-black" name="show"  data-width="100%" data-live-search="true" title="Select Job Type">
                               
                                <option value="0">yes</option>
            
                                <option value="1">no</option>
                                           
                            </select>
                            
                            
                        </div>
                    </div>
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 "><br>
                        <button class="btn btn-primary" type="submit">Update</button>
                    </div>
                </div>
            </form>
            <form class="needs-validation form-group" method="post" action="{{ url('admin_password_update') }}" role="form">
                @csrf()
                 <div class="row">
                <h2>Password Update</h2> 
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                        <label>Old password</label><br>
                        <div class="input-group">
                            
                            <input type="password" class="form-control" id="validationCustomUsername" name="old_password"  value="{{ old('username') }}" placeholder="old password" aria-describedby="inputGroupPrepend" >
                        </div>
                    </div><br>
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                        <br><label>New password</label><br>
                        <div class="input-group">
                            
                            <input type="password" class="form-control" id="validationCustomUsername" name="password"  value="{{ old('username') }}" placeholder="new passwordd" aria-describedby="inputGroupPrepend" >
                        </div>
                    </div>
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 "><br>
                        <button class="btn btn-primary" type="submit">Update</button>
                    </div>
                </div>
            </form>
            <h1>Telegram Setting</h1><br>
            <h3>To Add Your Telegram Number or to change it<br>
            Send this code<span style="color:red;"> 16002322 </span>to<span style="color:blue;"> @activecodebot</span></h3>
            </div>
            <div class="col-xl-6 col-lg-6"><br><br>
                <form class="needs-validation form-group" novalidate method="POST" action="{{ url('admi_send_sold') }}" role="form">
                            @csrf()
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                            <h2>Currency</h2>
                           
                                <input type="hidden" name="reselller_name"  value="">
                           
                                   
                                <label>Currency</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="inputGroupPrepend">$</span>
                                    </div>
                                    <input type="text" class="form-control" id="validationCustomUsername" name="sold"  value="DA" placeholder="Username" aria-describedby="inputGroupPrepend" required>
                                    <div class="invalid-feedback">
                                        Please choose a username.
                                    </div>
                                </div>
                        </div>
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                            <br>
                            <div class="input-group">
                                <button class="btn btn-primary" type="submit">.::. Update .::.</button>
                            </div>
                        </div>
                    </div>
                </form> 
                                    
                                        
                <form class="needs-validation form-group" novalidate method="POST" action="{{ url('bein_sports') }}" role="form">
                
                    @csrf()
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                        <h2>BeIN Sport</h2>
                        @foreach($subcat as $subcat)
                        <label>{{$subcat->name}}</label>
                        <div class="input-group">
                        <input type="hidden" class="form-control" id="validationCustomUsername" name="name[]"  value="{{$subcat->name}}" placeholder="Username" aria-describedby="inputGroupPrepend" >               
                        <input type="hidden" class="form-control" id="validationCustomUsername" name="ids[]"  value="{{$subcat->ids}}" placeholder="Username" aria-describedby="inputGroupPrepend" required>

                            <input type="number" class="form-control" id="validationCustomUsername" name="pri[]"  value="{{$subcat->price}}" placeholder="0.00" aria-describedby="inputGroupPrepend" required>
                            <div class="invalid-feedback">
                                Please choose a username.
                            </div>
                        </div>
                        @endforeach
                        </div>
                        
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                            <br>
                            <div class="input-group">
                                <button class="btn btn-primary" type="submit">.::. Update .::.</button>
                            </div>
                        </div>
                    </div>
                </form> 
                @foreach($subcat2 as $array)
                <form class="needs-validation form-group" novalidate method="POST" action="{{ url('sub_cat_pri')}}" role="form">
                @endforeach    
                    @csrf()
                    <div class="row">
                    
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                            <h2>Price</h2>
                            @foreach($subcat2 as $subcat2)
                            <label>{{$subcat2->name}}</label>
                            <div class="input-group">
                                <input type="hidden" class="form-control" id="validationCustomUsername" name="ids[]"  value="{{$subcat2->ids}}" placeholder="Username" aria-describedby="inputGroupPrepend" required>
                                <input type="hidden" class="form-control" id="validationCustomUsername" name="name[]"  value="{{$subcat2->name}}" placeholder="Username" aria-describedby="inputGroupPrepend" required>            
                                <input type="number" class="form-control" id="validationCustomUsername" name="pri[]"  value="{{$subcat2->price}}" placeholder="0.00" aria-describedby="inputGroupPrepend" required>
                                <div class="invalid-feedback">
                                    Please choose a username.
                                </div>
                            </div>
                            @endforeach
                        </div>
                   
                       
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                            <br>
                            <div class="input-group">
                                <button class="btn btn-primary" type="submit">.::. Send .::.</button>
                            </div>
                        </div>
                    </div>
                </form>                  
            </div>
        </div> 
        
    </div>
</div>
@endsection