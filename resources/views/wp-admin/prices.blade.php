@extends('wp-admin.aside')
@section('prices')active @endsection
@section('content')



<div class="dashboard-wrapper">
            <div class="container-fluid  dashboard-content">
            <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="page-header">
                            <p class="pageheader-title heading-size">Prices</p>
                            <p class="pageheader-text">Proin placerat ante duiullam scelerisque a velit ac porta, fusce sit amet vestibulum mi. Morbi lobortis pulvinar quam.</p>
                            <div class="page-breadcrumb bg-light">
                                <nav aria-label="breadcrumb ">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><i class="fa fa-chart-pie"></i><a href="{{url('user_index')}}" class="breadcrumb-link"> Dashboard</a></li>
                                        <li class="breadcrumb-item"><i class="fa fa-table"></i><a href="{{url('prices')}}" class="breadcrumb-link"> Prices</a></li>
                                        <!-- <li class="breadcrumb-item"><i class="fa fa-table"></i><a href="/wp-admin/add-new-package" class="breadcrumb-link"> Add new package</a></li> -->
                                    </ol>
                                </nav>
                            </div>
                        </div>

                        
                    </div>
                </div>


                    <div class="row">
                    <!-- ============================================================== -->
                    <!-- basic table  -->
                    <!-- ============================================================== -->
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-striped ">
                                    <thead>
                                    @foreach($datae as $row)
                                    <tr class="warning">
                                        <th>{{$row->name}}</th>
                                        <th>{{$row->price}}</th>
                                    </tr>
                                    @endforeach
									 
                            </thead>
                                    </table>
                                    </div>
                                    </div>
                                    </div>
                                    </div>

                                    </div>

                                    </div>
</div>
@endsection