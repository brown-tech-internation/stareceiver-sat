@extends('wp-admin.aside')
@section('add_cat')active @endsection

@section('content')
<div class="dashboard-wrapper">
    <div class="container-fluid  dashboard-content">
            <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="page-header">
                            <p class="pageheader-title heading-size">Add Code</p>
                            <p class="pageheader-text">Proin placerat ante duiullam scelerisque a velit ac porta, fusce sit amet vestibulum mi. Morbi lobortis pulvinar quam.</p>
                            <div class="page-breadcrumb bg-light">
                                <nav aria-label="breadcrumb ">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><i class="fa fa-chart-pie"></i><a href="{{url('wp-admin')}}" class="breadcrumb-link"> Dashboard</a></li>
                                        
                                        <!-- <li class="breadcrumb-item active" aria-current="page">Data Tables</li> -->
                                    </ol>
                                  
                                </nav>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="row">
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                <div class="card">
                @if(Session::has('success'))
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                    @php
                    Session::forget('success');
                    @endphp
            </div>
            @endif
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
                    <div class="card-body">
                        <form class="needs-validation form-group" novalidate method="POST" action="{{ url('add_sub_cat') }}" role="form">
                            @csrf()
                            
                            <div class="row ">
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 "> 
                                <label>ADD SUB CATEGORY</label> 
                                <div class="form-group">
                                    <select class="form-control selectpicker border rounded" name="id" id="job-type" data-style="btn-black" name="usertype"  data-width="100%" data-live-search="true" title="Select Job Type">
                                    @foreach($data as $row)   
                                    <option value="{{$row->id}}">{{$row->cat}}</option>
                                    @endforeach  
                                    </select>
                                </div>
                                </div>
                                

                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                    <div class="input-group">
                                        <input type="text" class="form-control" name="name" id="validationCustomUsername" placeholder="Enter Sub category" aria-describedby="inputGroupPrepend" required>
                                        <div class="invalid-feedback">
                                            Please choose a code.
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 "><br>
                                    <input type="hidden" class="form-control" name="price" value="0" id="validationCustom01" placeholder="Price"  required>
                                        <div class="valid-feedback">
                                            Looks good!
                                        </div>
                                </div><br>
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                <br>
                                    <button class="btn btn-primary" type="submit">Add</button>
                                </div>  
                            </div>
                        </form>
                    </div>
                </div>
            </div>
                        <!-- ============================================================== -->
                        <!-- end validation form -->
                        <!-- ============================================================== -->
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 "> 
            <div class="card-body">
            @if(Session::has('successed'))
                <div class="alert alert-success">
                    {{ Session::get('successed') }}
                    @php
                    Session::forget('successed');
                    @endphp
                </div>
            @endif
            <?php
                        $var=0;
                        ?>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered first">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Catogery</th>
                                <th>Type</th>
                                <th>Price</th>
                                <th>DELETE</th>
                            </tr>
                        </thead>
                        <tbody>
                       
					    @foreach($datas as $row)
                        <tr class="warning">
                        <td><?php echo ++$var ?></td>
                        <td>{{$row->cat}}</td>
                        <td>{{$row->name}}</td>
                        <td>{{$row->price}}</td>
                        <td><center><a href="{{ url('sub_cat',['id'=>$row->ids])}}"><button type="submit" class="btn btn-danger" style="width:54%">Delete</button></a></center></td>
                        </tr>
                        @endforeach
									
                    </table>
                </div>    
            </div>
            </div>            
        </div>
    </div>
</div>    



</div>
</div>
@endsection