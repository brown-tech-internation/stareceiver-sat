@extends('wp-admin.aside')
@section('main')active @endsection
@section('content')

<head>
<link rel="icon" href="assets/img/brand/favicon.png" type="image/png">
  <!-- Fonts -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
  <!-- Icons -->
  <link rel="stylesheet" href="assets/css/argon.css?v=1.2.0" type="text/css">
  <link rel="stylesheet" href="assets/vendor/nucleo/css/nucleo.css" type="text/css">
</head>
<div class="dashboard-wrapper">
  <div class="dashboard-ecommerce">
    <div class="container-fluid dashboard-content ">
                    <!-- ============================================================== -->
                    <!-- pageheader  -->
                    <!-- ============================================================== -->
      <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
          <div class="page-header">
            <h2 class="pageheader-title">Dashboard <small style="color:#777">Control Panel</small> </h2>
            <p class="pageheader-text">Nulla euismod urna eros, sit amet scelerisque torton lectus vel mauris facilisis faucibus at enim quis massa lobortis rutrum.</p>
            <div  class="page-breadcrumb">
                               
							<ol class="breadcrumb">
                <li class="active">
                <i class="fa fa-chart-pie"></i> Dashboard
                </li>
              </ol>
                                   
            </div>
          </div>
        </div>
      </div>


			<div class="alert alert-info alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <i class="fa fa-info-circle"></i>  <strong>يرجي ربط حسابك بتطبيق تيليقرام لاستقبال الاشعارات  <a href="setrev.php">من هنا</a></strong>
      </div>


			<div class="alert alert-info alert-dismissable">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
          <i class="fa fa-info-circle"></i>  <strong>CCP 12271662 clé 86 ... Mohamed Saffah ... تم تطبيق الأسعار الجديدة حسب المبلغ المرسل ... Tel : 0778625935 / 0668269229 ... Skype : Reda STARECEIVER
      ---------------
      الكود الواحد 350 دج duplex iptv player - متوفر أكواد 12شهر</strong>
      </div>

      <div class="row">
      @foreach($data as $row)
        <div class="col-md-3">
          <div class="card card-stats">
          
            <!-- Card body -->
            <div class="card-body" style="background-color:green">
              <div class="row">
                <div class="col">
                        
                  <span class="text-success"><i class="fa fa-desktop fa-5x" style="float:left;color:white"></i></span>
                  <span class="text-nowrap"><h1  class="text-white">{{$row->name}}</h1></span>
                  <span class="text-nowrap"><h5  class="text-white">{{$row->atr}}</h5></span>
                </div>
                <div class="col-auto">
                  <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                        <i class="ni ni-active-40"></i>
                  </div>
                </div>
              </div>
            </div>
            <div style="background-color:#f5f5f5">
              <p class="mt-3 mb-0 text-sm">
              <a href="{{ url('')}}">
              <span class="text-nowrap text-black ml-2 px-4" >View Details</span>
              <span class="text-success"><i class="fa fa-arrow-right text-primary "></i></span>
              </a>
              </p>
              <br>
            </div>
                
          </div>            
        </div>
      @endforeach   
    </div> 
  </div>
</div>
      
@endsection



