@extends('wp-admin.aside')
@section('reseller')active @endsection

@section('content')

<div class="dashboard-wrapper">
    <div class="container-fluid  dashboard-content">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="page-   1header">
                    @foreach($datas as $row)
                        <p class="pageheader-title heading-size">{{$row->email}}</p>
                    @endforeach
                    <p class="pageheader-text">Proin placerat ante duiullam scelerisque a velit ac porta, fusce sit amet vestibulum mi. Morbi lobortis pulvinar quam.</p>
                    <div class="page-breadcrumb bg-light">
                        <nav aria-label="breadcrumb ">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><i class="fa fa-chart-pie"></i><a href="/wp-admin" class="breadcrumb-link"> Dashboard</a></li>
                                <li class="breadcrumb-item"><i class="fa fa-table"></i><a href="/wp-admin/reseller" class="breadcrumb-link"> Your Resellers</a></li>
                                        <!-- <li class="breadcrumb-item active" aria-current="page">Data Tables</li> -->
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-12">
                @if(Session::has('success'))
                    <div class="alert alert-success">                             
                        {{ Session::get('success') }}
                        @php
                        Session::forget('success')
                        @endphp        
                    </div>
                @endif
            </div>
            <div class="col-xl-6 col-lg-6 col-md-6"><br><br>
                @foreach($datas as $row)
                    <form class="needs-validation form-group" novalidate method="POST" action="{{ url('reseller_update'  ,['id'=>$row->id]) }}" role="form">
                            @csrf()
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                            <h2>Update Reseller Information</h2>
                                        
                            <label>Username</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="inputGroupPrepend">@</span>
                                </div>
                                <input type="text" class="form-control" id="validationCustomUsername" name="username"  value="{{$row->email}}" placeholder="Username" aria-describedby="inputGroupPrepend" required>
                                <div class="invalid-feedback">
                                    Please choose a username.
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                            <br>
                            <label>Password</label>
                            <div class="input-group">
                                           
                                <input type="text" class="form-control" id="validationCustomUsername"  name="password" value="{{ $row->password }}" placeholder="Username" aria-describedby="inputGroupPrepend" required>
                                <div class="invalid-feedback">
                                    Please choose a username.
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                        
                            <br>   
                            <label>Mobile</label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="validationCustomUsername" name="mobile" value="{{ $row->phone }}" placeholder="Username" aria-describedby="inputGroupPrepend" required>
                                <div class="invalid-feedback">
                                    Please choose a username.
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                        
                            <br>   
                            <label>sold</label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="validationCustomUsername"  value="{{ $row->sold}}" placeholder="Username" aria-describedby="inputGroupPrepend"  disabled >
                                <div class="invalid-feedback">
                                    Please choose a username.
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                            <br>
                                        
                            <div class="input-group">
                                           
                                <button class="btn btn-primary" type="submit">update</button>
                            </div>
                        </div>
                    </form>                        
                @endforeach    
            </div>

            <div class="col-xl-6 col-lg-6"><br><br>
                <form class="needs-validation form-group" novalidate method="POST" action="{{ url('admi_send_sold') }}" role="form">
                            @csrf()
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                            <h2>Send sold</h2>
                            @foreach($datas as $row)
                                <input type="hidden" name="reselller_name"  value="{{$row->id}}">
                            @endforeach
                                   
                                <label>Username</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="inputGroupPrepend">$</span>
                                    </div>
                                    <input type="text" class="form-control" id="validationCustomUsername" name="sold"  value="" placeholder="Username" aria-describedby="inputGroupPrepend" required>
                                    <div class="invalid-feedback">
                                        Please choose a username.
                                    </div>
                                </div>
                        </div>
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                            <br>
                            <div class="input-group">
                                <button class="btn btn-primary" type="submit">.::. Send .::.</button>
                            </div>
                        </div>
                    </div>
                </form>                         
                <form class="needs-validation form-group" novalidate method="POST" action="{{ url('admin_send_pack') }}" role="form">
                    @csrf()
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                            <h2>Packeges</h2>
                                @foreach($datas as $row)
                                    <input type="hidden" name="user_id"  value="{{$row->id}}">
                                @endforeach
                                        
                                <label>Username</label>
                                <div class="input-group">
                                    <select class="form-control selectpicker border rounded" id="job-type" data-style="btn-black" name="p_id"  data-width="100%" data-live-search="true" title="Select Job Type">
                                        <option>select package</option>
                                        @foreach($pack as $row1)
                                        <option value="{{$row1->p_id}}">{{$row1->p_name}}</option>
                                        @endforeach    
                                   </select>
                                 </div>
                            </div>
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                            <br>
                            <div class="input-group">
                                <button class="btn btn-primary" type="submit">.::. Send .::.</button>
                            </div>
                        </div>
                    </div>
                </form>
                <form class="needs-validation form-group" novalidate method="POST" action="{{ url('reseller_save') }}" role="form">
                    @csrf()
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                        <h2>BeIN Sport</h2>
                        <label>TOP Sport 1 Month (2550)</label>
                        <div class="input-group">
                                            
                            <input type="number" class="form-control" id="validationCustomUsername" name="username"  value="{{ old('username') }}" placeholder="Username" aria-describedby="inputGroupPrepend" required>
                            <div class="invalid-feedback">
                                Please choose a username.
                            </div>
                        </div>
                        </div>
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                       
                            <label>TOP Sport 1 Month (2550)</label>
                                <div class="input-group">
                                    <input type="number" class="form-control" id="validationCustomUsername" name="username"  value="{{ old('username') }}" placeholder="Username" aria-describedby="inputGroupPrepend" required>
                                    <div class="invalid-feedback">
                                        Please choose a username.
                                    </div>
                                </div>
                                        
                                       
                        </div>
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                        
                            <label>TOP Sport 1 Month (2550)</label>
                            <div class="input-group">
                                            
                                <input type="number" class="form-control" id="validationCustomUsername" name="username"  value="{{ old('username') }}" placeholder="Username" aria-describedby="inputGroupPrepend" required>
                                <div class="invalid-feedback">
                                    Please choose a username.
                                </div>
                            </div>
                                        
                                    
                        </div>
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                            <br>
                            <div class="input-group">
                                <button class="btn btn-primary" type="submit">.::. Send .::.</button>
                            </div>
                        </div>
                    </div>
                </form> 
                <form class="needs-validation form-group" novalidate method="POST" action="{{ url('reseller_save') }}" role="form">
                    @csrf()
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                            <h2>Price</h2>
                            <label>SDS (1919)</label>
                            <div class="input-group">
                                            
                                <input type="number" class="form-control" id="validationCustomUsername" name="username"  value="{{ old('username') }}" placeholder="Username" aria-describedby="inputGroupPrepend" required>
                                <div class="invalid-feedback">
                                    Please choose a username.
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                       
                            <label>SDS (1919)</label>
                            <div class="input-group">
                                            
                                <input type="number" class="form-control" id="validationCustomUsername" name="username"  value="{{ old('username') }}" placeholder="Username" aria-describedby="inputGroupPrepend" required>
                                <div class="invalid-feedback">
                                    Please choose a username.
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                        
                            <label>SDS (1919)</label>
                                <div class="input-group">
                                            
                                    <input type="number" class="form-control" id="validationCustomUsername" name="username"  value="{{ old('username') }}" placeholder="Username" aria-describedby="inputGroupPrepend" required>
                                    <div class="invalid-feedback">
                                        Please choose a username.
                                    </div>
                                </div>
                        </div>
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                            <br>
                            <div class="input-group">
                                <button class="btn btn-primary" type="submit">.::. Send .::.</button>
                            </div>
                        </div>
                    </div>
                </form>                  
            </div>
        </div>    
    </div>
</div>
            
          

@endsection