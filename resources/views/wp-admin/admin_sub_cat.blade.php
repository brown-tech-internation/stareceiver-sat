@extends('wp-admin.aside')
@section('main')active @endsection
@section('content')

<head>
<link rel="icon" href="assets/img/brand/favicon.png" type="image/png">
  <!-- Fonts -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
  <!-- Icons -->
  <link rel="stylesheet" href="assets/css/argon.css?v=1.2.0" type="text/css">
  <link rel="stylesheet" href="assets/vendor/nucleo/css/nucleo.css" type="text/css">
</head>
<div class="dashboard-wrapper">
            <div class="dashboard-ecommerce">
                <div class="container-fluid dashboard-content ">
                    <!-- ============================================================== -->
                    <!-- pageheader  -->
                    <!-- ============================================================== -->
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="page-header">
                                <h2 class="pageheader-title">Dashboard <small style="color:#777">Control Panel</small> </h2>
                                <p class="pageheader-text">Nulla euismod urna eros, sit amet scelerisque torton lectus vel mauris facilisis faucibus at enim quis massa lobortis rutrum.</p>
                                <div  class="page-breadcrumb">
                               
									<ol class="breadcrumb">
                                  <li class="active">
                                  <i class="fa fa-chart-pie"></i> Dashboard
                                   </li>
                                  </ol>
                                   
                                </div>
                            </div>
                        </div>
                    </div>




            <div class="row">
            <div class="col-xl-12 col-md-12">
            @if(Session::has('success'))
                                    <div class="alert alert-success">
                                    {{ Session::get('success') }}
                                    @php
                                    Session::forget('success');
                                    @endphp
                                    </div>
                                @endif
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif
            </div>                    
            @php $k=0; @endphp
           
            @foreach($users as $row8)
           
            @php ++$k; @endphp 
              
                
                <div class="col-xl-4 col-md-6">
                    <div class="card card-stats">
                        <div class="card-body" 
                        @if(($row8->cat)=='DONGEL')
                        style="background-color:BROWN;"
                        @elseif(($row8->cat)=='VOD')
                        style="background-color:#f0ad4e;"
                        @elseif(($row8->cat)=='SERVERS')
                        style="background-color:green;"
                        @elseif(($row8->cat)=='GIFT CARDS')
                        style="background-color:yellow;"
                        @elseif(($row8->cat)=='BEIN SPORTS')
                        style="background-color:#e0b0ff;"
                        @elseif(($row8->cat)=='NETFLIX ACCOUNTS')
                        style="background-color:red;"
                        @elseif(($row8->cat)=='MOBILE & INTERNET')
                        style="background-color:orange;"
                       @endif
                       style="background-color:blue;"
                       >
                            <div class="row">
                                <div class="col">
                        
                                    <span class="text-success"><i class="fa fa-desktop fa-5x" style="float:left;color:white"></i></span>
                                    <span class="text-nowrap"><h2  class="text-white">{{$row8->name}}</h2></span>
                                </div>
                                <div class="col-auto">
                                    <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                                    <i class="ni ni-active-40"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="background-color:#f5f5f5">
                            <p class="mt-3 mb-0 text-sm">
                            <a href="{{url(''  ,['id'=>$row8->id])}}" data-toggle="modal" data-target="#myModal1{{$k}}" >
                                <span class="text-nowrap text-black ml-2 px-4" >View Details</span>
                            <span class="text-success"><i class="fa fa-arrow-right text-primary "></i></span>
                            </a>
                            </p>
                            <br>
                        </div>
                    </div>
                </div> 
                <div class="modal" id="myModal1{{$k}}">
                  <div class="modal-dialog">
                    <div class="modal-content">
      
                      <div class="modal-header">
                        <h4 class="modal-title">{{$row8->name}}</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                      </div>
                   
                                <!-- Modal body -->
                      <div class="modal-body">
                    
                                
                        <form  method="POST" action="{{ url('admin_order_code') }}" role="form">
                            @csrf()
                           
                          <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                               	
                          </div>
                               
                          <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                            <br>
                            <input type="hidden" value="{{$row8->ids}}" name="ids"> 
                            
                            <div class="form-group">
                                <label>Number of Codes:</label>               
                                <select class="form-control selectpicker border rounded" id="job-type" data-style="btn-black" name="no"  data-width="100%" data-live-search="true" title="Select Job Type">
                                    <option value=1>1</option>
                                    <option value=3>3</option>
                                    <option value=5>5</option>
                                </select>
                            </div>
                          </div><br>
                          <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
         
                            <button type="submit" class="btn btn-primary">Order</button>
                          </div>    
                        </form>
                      </div>
        
                                <!-- Modal footer -->
                      <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                      </div>
                    </div>
                  </div>
                </div>
              
               
            @endforeach                 
            </div>
        </div>
    </div>
</div>

@endsection