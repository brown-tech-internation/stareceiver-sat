@extends('wp-admin.aside')
@section('code')active @endsection

@section('content')

<div class="dashboard-wrapper">
            <div class="container-fluid  dashboard-content">
                <!-- ============================================================== -->
                <!-- pageheader -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="page-header">
                            <p class="pageheader-title heading-size">Your Codes</p>
                            <p class="pageheader-text">Proin placerat ante duiullam scelerisque a velit ac porta, fusce sit amet vestibulum mi. Morbi lobortis pulvinar quam.</p>
                            <div class="page-breadcrumb">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><i class="fa fa-chart-pie"></i><a href="{{url('wp-admin')}}" class="breadcrumb-link"> Dashboard</a></li>
                                        <li class="breadcrumb-item"><i class="fa fa-table"></i><a href="{{url('wp-admin/code')}}" class="breadcrumb-link"> code</a></li>
                                        <li class="breadcrumb-item"><i class="fa fa-table"></i><a href="{{url('code_purchase_his')}}" class="breadcrumb-link">purchase code</a></li>
                                        <!-- <li class="breadcrumb-item active" aria-current="page">Data Tables</li> -->
                                    </ol>
                                  
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- end pageheader -->
                <!-- ============================================================== -->
                <div class="row">
                    <!-- ============================================================== -->
                    <!-- basic table  -->
                    <!-- ============================================================== -->
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="card">
                        @if(Session::has('success'))
                                    <div class="alert alert-danger">
                                    {{ Session::get('success') }}
                                    @php
                                    Session::forget('success');
                                    @endphp
                                    </div>
                                @endif
                            
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered first">
                                        <thead>
                                            <tr>
                                                <th>Code</th>
                                                <th>Type</th>
                                                
                                                <th>Price</th>
                                                <th>Date</th>
                                                <th>SN</th>
                                                <th>Client Name</th>
                                               
                                            </tr>
                                        </thead>
                                        <tbody>
								
                                        @php $k=0; @endphp
                                          
                                           
          
                                           
								
                                    @foreach($datae as $row)
                                    @php ++$k; @endphp
                                    <tr class="warning">
                                        <td> <a href="" data-toggle="modal" data-target="#myModal1{{$k}}" >{{$row->code}}</a></td>
                                        <td>{{$row->name}}</td>
                                        <td>{{$row->price}}</td>
										
										<td>{{$row->created_at}}</td>
										<td>{{$row->SN}}</td>
										<td>{{$row->clint_name}}</td>
                                        <div class="modal" id="myModal1{{$k}}">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                        <h4 class="modal-title"></h4>
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        </div>
                   
                                                        <div class="modal-body">
                    
                                
                                                            <form  method="POST" action="{{ url('admin_update_code' ,['ad_id'=>$row->ad_id]) }}" role="form">
                                                                @csrf()
                           
                                                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                	
                                                                </div>
                               
                                                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                                                    <div class="input-group">
                                                                        <input type="text" class="form-control" name="cn"  value="{{$row->SN}}" placeholder="SN" aria-describedby="inputGroupPrepend" >
                                                  
                                                                    </div>
                                                                </div>
                                                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 "><br>
                                                                    <div class="input-group">
                                                                       
                                                                        <input type="text" class="form-control" name="clint_name"  value="{{$row->clint_name}}" placeholder="Client Name" aria-describedby="inputGroupPrepend" >
                                                  
                                                                    </div>
                                                                </div>
                                                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 "><br>
         
                                                                    <button type="submit" class="btn btn-primary">Update</button>
                                                                </div>    
                                                            </form>
                                                        </div>
        
                                
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                       


                                    </tr>
                                    @endforeach
									
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- ============================================================== -->
                    <!-- end basic table  -->
                    <!-- ============================================================== -->
                </div>
               <!-- ============================================================== -->
        </div>


@endsection