@extends('wp-admin.aside')
@section('reseller')active @endsection

@section('content')

<div class="dashboard-wrapper">
            <div class="container-fluid  dashboard-content">
                <!-- ============================================================== -->
                <!-- pageheader -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="page-   1header">
                            <p class="pageheader-title heading-size">Your Resellers</p>
                            <p class="pageheader-text">Proin placerat ante duiullam scelerisque a velit ac porta, fusce sit amet vestibulum mi. Morbi lobortis pulvinar quam.</p>
                            <div class="page-breadcrumb bg-light">
                                <nav aria-label="breadcrumb ">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><i class="fa fa-chart-pie"></i><a href="{{url('/wp-admin')}}" class="breadcrumb-link"> Dashboard</a></li>
                                        <li class="breadcrumb-item"><i class="fa fa-table"></i><a href="{{url('wp-admin/reseller')}}" class="breadcrumb-link">All Resellers</a></li>
                                        <li class="breadcrumb-item"><i class="fa fa-table"></i><a href="{{url('admin_reseller')}}" class="breadcrumb-link">your Resellers</a></li>
                                        <!-- <li class="breadcrumb-item active" aria-current="page">Data Tables</li> -->
                                    </ol>
                                  
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- end pageheader -->
                <!-- ============================================================== -->
                <div class="row">
                    <!-- ============================================================== -->
                    <!-- basic table  -->
                    <!-- ============================================================== -->
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12"><br>
                        <div class="card">
                        @if(Session::has('success'))
                                    <div class="alert alert-danger">
                                    {{ Session::get('success') }}
                                    @php
                                    Session::forget('success');
                                    @endphp
                                    </div>
                                @endif
                        
                            
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered first">
                                        <thead>
                                            <tr>
                                                <th>Reseller</th>
                                                <th>Password</th>
                                                <th>Gros</th>
                                                <th>Sold</th>
                                                <th>2FA</th>
                                                <th>Formate 2FA</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
								
                                    @foreach($users as $row)	
									<tr class="active">
                                  
                                       <td>  <a href="{{url('reslller_info'  ,['id'=>$row->id])}}">{{$row->email}}</a></td>
                                       <td>{{$row->password}}</td>
                                       <td>{{$row->addby}}</td>
                                       <td>
                                       @if (($row->sold)==null)
                                        0
                                       @else
                                       {{$row->sold}}
                                    
                                       @endif</td>
                                       <td> @if (($row->state)==null)
                                        Active
                                       @else
                                       Deactivate
                                    
                                       @endif</td>
                                       <td></td>
                                       <td><div class="dropdown">
                                            <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Action
                                            <span class="caret"></span></button>
                                            <ul class="dropdown-menu">
                                            @if (($row->state)==null)
                                            <li><a href="{{url('disactive'  ,['id'=>$row->id])}}"">Deactivate</a></li> 
                                                
                                            @else
                                            <li><a href="{{url('active'  ,['id'=>$row->id])}}"">Active</a></li>   
                                                
                                            @endif    
                                                <li><a href="{{url('delete_user'  ,['id'=>$row->id])}}"">Delete</a></li>
                                            </ul>
                                        </div>
                                       </td>
                                      
                                    </tr>
									@endforeach
									</tbody>
                                       
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- ============================================================== -->
                    <!-- end basic table  -->
                    <!-- ============================================================== -->
                </div>
               <!-- ============================================================== -->
        </div>


@endsection