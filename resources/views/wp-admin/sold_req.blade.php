@extends('wp-admin.aside')
@section('sold')active @endsection

@section('content')

<div class="dashboard-wrapper">
            <div class="container-fluid  dashboard-content">
                <!-- ============================================================== -->
                <!-- pageheader -->
                <!-- ============================================================== -->
                <div class="row">
                               
                    <div class="container">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" style="margin-top:2%;">
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif

                                @if(Session::has('success'))
                                    <div class="alert alert-success">
                                    {{ Session::get('success') }}
                                    @php
                                    Session::forget('success');
                                    @endphp
                                    </div>
                                @endif
                        </div> 
                       

                  
                    

                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="page-header">
                            <p class="pageheader-title heading-size">Sold Not Paied</p>
                            <p class="pageheader-text">Proin placerat ante duiullam scelerisque a velit ac porta, fusce sit amet vestibulum mi. Morbi lobortis pulvinar quam.</p>
                            <div class="page-breadcrumb bg-light">
                                <nav aria-label="breadcrumb ">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><i class="fa fa-chart-pie"></i><a href="{{url('wp-admin')}}" class="breadcrumb-link"> Dashboard</a></li>
                                        <li class="breadcrumb-item"><i class="fa fa-table"></i><a href="{{url('wp-admin/sold')}}" class="breadcrumb-link"> sold</a></li>
                                        <li class="breadcrumb-item"><i class="fa fa-table"></i><a href="{{url('sold_req')}}" class="breadcrumb-link"> Sold Request</a></li>

                                        <!-- <li class="breadcrumb-item active" aria-current="page">Data Tables</li> -->
                                    </ol>
                                  
                                </nav>
                            </div>
                        </div>
                    </div>
               
                    <!-- ============================================================== -->
                    <!-- basic table  -->
                    <!-- ============================================================== -->
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="card">
                            <center><h2>Sold Request</h2></center>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered first">
                                        <thead>
                                            <tr>
                                                <th>Reseller</th>
                                                <th>sold</th>
                                                <th>Date</th>
                                                <th>PAY</th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
								
                                    @foreach($dataes as $row1)
									<tr class="active">
                                       <td>{{$row1->email}}</td>
                                       <td>{{$row1->soldss}}</td>
                                       <td><?php echo date($row1->created_at);?></td>
                                       <td><form class="needs-validation form-group" method="post" action="{{ url('sold_req_approve' ,['id'=>$row1->id,'ids'=>$row1->ids]) }}" role="form">
                                    @csrf()
                                    <input type="hidden" class="form-control" id="validationCustomUsername" name="sold"  value="{{$row1->soldss}}" placeholder="Username" aria-describedby="inputGroupPrepend" >
                                    <button class="btn btn-primary" type="submit">PAY</button>
                                    </form>             
                
                        </td>
                                    </tr>
									@endforeach
									</tbody>
                                       
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- ============================================================== -->
                    <!-- end basic table  -->
                    <!-- ============================================================== -->
                </div>
            </div>
            </div>
            </div>


@endsection