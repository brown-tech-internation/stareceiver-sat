@extends('wp-admin.aside')
@section('add-codes')active @endsection


@section('content')


<div class="dashboard-wrapper">

            <div class="container-fluid  dashboard-content">
            <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="page-header">
                            <p class="pageheader-title heading-size">Add Code</p>
                            <p class="pageheader-text">Proin placerat ante duiullam scelerisque a velit ac porta, fusce sit amet vestibulum mi. Morbi lobortis pulvinar quam.</p>
                            <div class="page-breadcrumb bg-light">
                                <nav aria-label="breadcrumb ">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><i class="fa fa-chart-pie"></i><a href="wp-admin" class="breadcrumb-link"> Dashboard</a></li>
                                        <li class="breadcrumb-item"><i class="fa fa-table"></i><a href="/wp-admin/add-codes" class="breadcrumb-link"> Add Code</a></li>
                                        <!-- <li class="breadcrumb-item active" aria-current="page">Data Tables</li> -->
                                    </ol>
                                  
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                   
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="card">
                               
                                <div class="card-body">
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif

                                @if(Session::has('success'))
                                    <div class="alert alert-success">
                                    {{ Session::get('success') }}
                                    @php
                                    Session::forget('success');
                                    @endphp
                                    </div>
                                @endif

                                    <form class="needs-validation form-group" novalidate method="POST" action="{{ url('code_save') }}" role="form">
                                    @csrf()
                                        <div class="row ">
                                            
                                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                              
                                                <div class="input-group">
                                                    
                                                    <textarea  class="form-control" name="code" id="validationCustomUsername" placeholder="Enter Code" aria-describedby="inputGroupPrepend" required></textarea>
                                                    
                                                    
                                                    <div class="invalid-feedback">
                                                        Please choose a code.
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                                <div class="form-group">
                                                    <br>
                                                    <select class="form-control formselect required" name="id" placeholder="Select Category" id="country">
                                                        <option value="0" disabled selected>Select
                                                         Main Category*</option>
                                                        @foreach($datas as $categories)
                                                        <option  value="{{ $categories->id }}">
                                                        {{ ucfirst($categories->cat) }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                                <div class="form-group">
                                                    <br>
                                                    
                                                    <select class="form-control formselect required" name="ids" placeholder="Select Sub Category"  name="ids" id="state">
                                                    </select>
                                                </div>
                                            </div>



                                            
                                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 "><br>
                                                
                                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 ">
                                                <button class="btn btn-primary" type="submit">Add</button>
                                            </div>  
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- ============================================================== -->
                        <!-- end validation form -->
                        <!-- ============================================================== -->
                    </div>
                    </div>
               <!-- ============================================================== -->
<script src="{{asset('js/coderman.js')}}"></script>
               
<script type=text/javascript>
  $('#country').change(function(){
  var countryID = $(this).val();  
 
    $.ajax({
      type:"GET",
      url:"{{url('sub_cat_code')}}?country_id="+countryID,
      success:function(res){        
      
        $("#state").empty();
        $("#state").append('');
        $.each(res,function(key,value){
          $("#state").append('<option value="'+key+'">'+value+'</option>');
        });
      
      }
    });
 
  });
</script>
@endsection