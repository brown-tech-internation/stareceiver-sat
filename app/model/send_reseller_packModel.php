<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class send_reseller_packModel extends Model
{
    protected $table = "send_reseller_pack";
    public $timestamps = false;
    // protected $primaryKey = 'id';
}
