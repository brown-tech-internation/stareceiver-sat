<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class admin_code_purModel extends Model
{
    //
    protected $table = "admin_code_pur";
    public $timestamps = false;
    protected $primaryKey ='ad_id';
}
