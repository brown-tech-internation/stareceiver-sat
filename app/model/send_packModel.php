<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class send_packModel extends Model
{
    protected $table = "send_pack";
    public $timestamps = false;
    protected $primaryKey = 'id';

}

