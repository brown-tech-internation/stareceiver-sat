<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class packageModel extends Model
{
    protected $table = "package";
    public $timestamps = false;
    protected $primaryKey = 'p_id';

}
