<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class advertisementModel extends Model
{
    protected $table = "advertisement";
    public $timestamps = false;
}
