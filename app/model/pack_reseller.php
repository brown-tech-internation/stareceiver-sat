<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class pack_reseller extends Model
{
    
    protected $table = "pack_reseller";
    public $timestamps = false;
    protected $primaryKey ='pr_id';

}
