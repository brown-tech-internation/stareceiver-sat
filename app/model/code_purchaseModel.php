<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class code_purchaseModel extends Model
{
    
    
    
    protected $table = "code_purchase";
    public $timestamps = false;
    protected $primaryKey ='cp_id';
}
