<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class user_code_purModel extends Model
{
    protected $table = "user_code_pur";
    public $timestamps = false;
    protected $primaryKey ='ucp_id';

}
