<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class code_purchase_history extends Model
{
    //
    protected $table = "code_purchase_his";
    public $timestamps = false;
    protected $primaryKey ='chp_id';
    
}
