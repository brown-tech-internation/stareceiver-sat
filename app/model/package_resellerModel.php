<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class package_resellerModel extends Model
{
    protected $table = "package_reseller";
    public $timestamps = false;
    protected $primaryKey = 'pr_id';

}
