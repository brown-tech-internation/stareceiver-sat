<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class addbyModel extends Model
{
    
    protected $table = "addby";
    public $timestamps = false;
    protected $primaryKey = 'u_id';

}
