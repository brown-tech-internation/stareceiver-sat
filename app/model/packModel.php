<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class packModel extends Model
{
    protected $table = "pack";
    public $timestamps = false;
    protected $primaryKey ='p_id';
}
