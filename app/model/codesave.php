<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class codesave extends Model
{
    protected $table = "code_save";
    public $timestamps = false;

    protected $primaryKey = 'Nid';
}
