<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\soldModel;
use App\Model\soldhistroyModel;
use App\Model\AdminModel;
use App\Model\catModel;
use App\Model\sub_catModel;
use App\Model\advertisementModel;
use App\Model\code_purchaseModel;
use App\Model\addbyModel;
use App\Model\user_code_purModel; 


use DB;

class userController extends Controller
{
    function user_index()
    {
        $data = session("email");
        $id = session("id");
        
        return view('user/user_index', compact('data' ,'datas'));
    }
    function dashboard()
    {
        $data = session("email");
        $id = session("id");
        $user =new AdminModel();
        $datas = $user->where('id' ,$id)->get();
        $add=advertisementModel::get();
        $use1 = catModel::where('id',1)->get();
        $DONGEL=DB::table('sub_cat')
        ->join('cat','sub_cat.id', '=','cat.id')
        ->join('pack_reseller','sub_cat.ids', '=','pack_reseller.ids')
        ->join('package_reseller','pack_reseller.pr_id', '=','package_reseller.pr_id')
        ->join('send_reseller_pack','package_reseller.pr_id', '=','send_reseller_pack.pr_id')
        ->where('send_reseller_pack.id',$id)
        ->where('cat.cat','=','DONGEL')
        ->get()->sum('r_pk_price');
        $use2 = catModel::where('id',2)->get();
        $SERVERS=DB::table('sub_cat')
        ->join('cat','sub_cat.id', '=','cat.id')
        ->join('pack_reseller','sub_cat.ids', '=','pack_reseller.ids')
        ->join('package_reseller','pack_reseller.pr_id', '=','package_reseller.pr_id')
        ->join('send_reseller_pack','package_reseller.pr_id', '=','send_reseller_pack.pr_id')
        ->where('send_reseller_pack.id',$id)
        ->where('cat.cat','=','SERVERS')
        ->get()->sum('r_pk_price');
        $use3 = catModel::where('id',3)->get();
        $IPT=DB::table('sub_cat')
        ->join('cat','sub_cat.id', '=','cat.id')
        ->join('pack_reseller','sub_cat.ids', '=','pack_reseller.ids')
        ->join('package_reseller','pack_reseller.pr_id', '=','package_reseller.pr_id')
        ->join('send_reseller_pack','package_reseller.pr_id', '=','send_reseller_pack.pr_id')
        ->where('send_reseller_pack.id',$id)
        ->where('cat.cat','=','IPTV')
        ->get()->sum('r_pk_price');
        $use4 = catModel::where('id',4)->get();
        $VOD=DB::table('sub_cat')
        ->join('cat','sub_cat.id', '=','cat.id')
        ->join('pack_reseller','sub_cat.ids', '=','pack_reseller.ids')
        ->join('package_reseller','pack_reseller.pr_id', '=','package_reseller.pr_id')
        ->join('send_reseller_pack','package_reseller.pr_id', '=','send_reseller_pack.pr_id')
        ->where('send_reseller_pack.id',$id)
        ->where('cat.cat','=','VOD')
        ->get()->sum('r_pk_price');
        $use5 = catModel::where('id',5)->get();
        $TEST=DB::table('sub_cat')
        ->join('cat','sub_cat.id', '=','cat.id')
        ->join('pack_reseller','sub_cat.ids', '=','pack_reseller.ids')
        ->join('package_reseller','pack_reseller.pr_id', '=','package_reseller.pr_id')
        ->join('send_reseller_pack','package_reseller.pr_id', '=','send_reseller_pack.pr_id')
        ->where('send_reseller_pack.id',$id)
        ->where('cat.cat','=','TEST')
        ->get()->sum('r_pk_price');
        $use6 = catModel::where('id',6)->get();
        $GIFTCARDS=DB::table('sub_cat')
        ->join('cat','sub_cat.id', '=','cat.id')
        ->join('pack_reseller','sub_cat.ids', '=','pack_reseller.ids')
        ->join('package_reseller','pack_reseller.pr_id', '=','package_reseller.pr_id')
        ->join('send_reseller_pack','package_reseller.pr_id', '=','send_reseller_pack.pr_id')
        ->where('send_reseller_pack.id',$id)
        ->where('cat.cat','=','GIFT CARDS')
        ->get()->sum('r_pk_price');
        $use7 = catModel::where('id',7)->get();
        $BEINSPORTS=DB::table('sub_cat')
        ->join('cat','sub_cat.id', '=','cat.id')
        ->join('pack_reseller','sub_cat.ids', '=','pack_reseller.ids')
        ->join('package_reseller','pack_reseller.pr_id', '=','package_reseller.pr_id')
        ->join('send_reseller_pack','package_reseller.pr_id', '=','send_reseller_pack.pr_id')
        ->where('send_reseller_pack.id',$id)
        ->where('cat.cat','=','BEIN SPORTS')
        ->get()->sum('r_pk_price');
        $use8 = catModel::where('id',8)->get();
        $NETFLIXACCOUNTS=DB::table('sub_cat')
        ->join('cat','sub_cat.id', '=','cat.id')
        ->join('pack_reseller','sub_cat.ids', '=','pack_reseller.ids')
        ->join('package_reseller','pack_reseller.pr_id', '=','package_reseller.pr_id')
        ->join('send_reseller_pack','package_reseller.pr_id', '=','send_reseller_pack.pr_id')
        ->where('send_reseller_pack.id',$id)
        ->where('cat.cat','=','NETFLIX ACCOUNTS')
        ->get()->sum('r_pk_price');
        $use9 = catModel::where('id',9)->get(); 
        $MOBILEINTERNET=DB::table('sub_cat')
        ->join('cat','sub_cat.id', '=','cat.id')
        ->join('pack_reseller','sub_cat.ids', '=','pack_reseller.ids')
        ->join('package_reseller','pack_reseller.pr_id', '=','package_reseller.pr_id')
        ->join('send_reseller_pack','package_reseller.pr_id', '=','send_reseller_pack.pr_id')
        ->where('send_reseller_pack.id',$id)
        ->where('cat.cat','=','MOBILE & INTERNET')
        ->get()->sum('r_pk_price');
        return view('user/dashboard',compact('data' ,'datas','use1','use2','use3','use4','use5','use6','use7','use8','use9','DONGEL','SERVERS','IPT','VOD','TEST','GIFTCARDS','BEINSPORTS','NETFLIXACCOUNTS','MOBILEINTERNET','add'));
    }
    function sub_cat_user($ids){
        $data = session("email");
        $id= session("id");
        $user =new AdminModel();
        $datas = $user->where('id' ,$id)->get();
        $datae=DB::table('sub_cat')
        ->join('cat','sub_cat.id', '=','cat.id')
        ->join('pack_reseller','sub_cat.ids', '=','pack_reseller.ids')
        ->join('package_reseller','pack_reseller.pr_id', '=','package_reseller.pr_id')
        ->join('send_reseller_pack','package_reseller.pr_id', '=','send_reseller_pack.pr_id')
        ->where('send_reseller_pack.id',$id)
        ->where('sub_cat.id','=',$ids)
        ->get();
        // dd($datae);
        $cat =catModel::where('id',$ids)->value('cat');
       
        return view('user/sub_cat1' ,compact('data' ,'datas','datae','cat'));

    }
    function code()
    {
        $data = session("email");
        $id = session("id");
        $user =new AdminModel();
        $datas = $user->where('id' ,$id)->get();
        $codes=DB::table('package_reseller')
        ->join('send_reseller_pack','package_reseller.pr_id', '=','send_reseller_pack.pr_id')
        ->join('pack_reseller','package_reseller.p_id', '=','pack_reseller.p_id')
        ->join('sub_cat','pack_reseller.ids', '=','sub_cat.ids')
        ->join('user_code_pur','sub_cat.ids', '=','user_code_pur.ids')
        ->where('send_reseller_pack.id',$id)
        ->where('user_code_pur.sess_id',$id)->orderBy('ucp_id', 'DESC')
        ->paginate(15);
        return view('user/code' ,compact('data' ,'datas','codes'));
    }
    function user_update_code(Request $request,$ucp_id){
        
        $use=user_code_purModel::findOrFail($ucp_id);
        $use->SN=$request->input('cn');
        $use->clint_name=$request->input('clint_name');
        $use->save();
        return back()->with('success', 'Code Update');
    }
    function search()
    {
        $data = session("email");
        $id = session("id");
        $user =new AdminModel();
        $datas = $user->where('id' ,$id)->get();
        return view('user/search',compact('data' ,'datas'));
    }
    function sold()
    {
        $data = session("email");
        $id = session("id");
        $user =new AdminModel();
        $datas = $user->where('id' ,$id)->get();
        $user1=new soldhistroyModel();
        $data4 = $user1->where('id' ,$id)->orderBy('ids', 'DESC')->get();
        // dd($data4);
        return view('user/sold' ,compact('data' ,'datas','data4'));
    }
    function demond()
    {
        $data = session("email");
        $data1 = session("id");
        $data2 = session("add");
       
        $id = session("id");
        $user =new AdminModel();
        $users =new soldhistroyModel();

        $datas = $user->where('id' ,$id)->get();
        $user1=new soldhistroyModel();
        $data4 = $user1->where('id' ,$id)->orderBy('ids', 'DESC')->get();
// dd($data4,$id);
        $data3 = $users->where('id' ,$id)->orderBy('ids', 'DESC')->paginate(12);
        
        //$id = session("id");

        // $user =new AdminModel();
        // $datas = $user->where('id' ,$id)->get();
        
        // $user1=new soldhistroyModel();
        // $data1 = $user1->where('id' ,$id)->orderBy('ids', 'DESC')->get();
       
    return view('user/demond', compact('data','data1','data2','datas','data3','data4'));
        
    }
    function charging()
    {
        $data = session("email");
        $id = session("id");
        $user =new AdminModel();
        $datas = $user->where('id' ,$id)->get();
        return view('user/charging'  ,compact('data' ,'datas'));
    }
    function packages()
    {
        $data = session("email");
        $id = session("id");
        $user =new AdminModel();
        $datas = $user->where('id' ,$id)->get();
        return view('user/packages' ,compact('data' ,'datas'));
    }
    function being_sport()
    {
        $data = session("email");
        $id = session("id");
        $user =new AdminModel();
        $datas = $user->where('id' ,$id)->get();
        return view('user/being_sport' ,compact('data' ,'datas'));
    }
    function prices()
    {
        $data = session("email");
        $id = session("id");
        $user =new AdminModel();
        $datas = $user->where('id' ,$id)->get();


        $pack=DB::table('send_reseller_pack')
        ->join('pack_reseller','send_reseller_pack.pr_id', '=','pack_reseller.pr_id')
        ->join('sub_cat','pack_reseller.ids', '=','sub_cat.ids')
        ->where('send_reseller_pack.id',$id)
        ->get();
       
        return view('user/prices' ,compact('data' ,'datas','pack'));
    }
    function demand_solds(Request $request)
    {
        
        $request->validate([
            'name'        =>      'required',
            'demand'         =>      'required'
            
            
        ]);
       $user = new soldModel();
       $users = new soldhistroyModel();
       $sid = session("id");
       $addby = DB::table("addby")
        ->where("u_id",$sid)
        ->value('ids');
       $user->email=$request->input('name');
       $user->id=$request->input('id');
       $user->soldss= $request->input('demand');
       $user->addby= $addby;
     


       $users->email=$request->input('name');
       $users->id=$request->input('id');
       $users->solds= $request->input('demand');
       $users->addby= $addby;
    
       $users->save();
       $user->save();

       if(!is_null($user)) {
        return back()->with('success', 'Your Demand successfully send.');
        }
        else {
        return back()->with('error', 'Whoops! some error encountered. Please try again.');
        }  
    }
    function setting()
    {
       $data = session("email");
       $id = session("id");
       $user =new AdminModel();
       $datas = $user->where('id' ,$id)->get();
       return view('user/settings', compact('data','datas'));
    }  
    function password_update(Request $request)
    {
       $id = session("id");
       $user =new AdminModel();
      
      
       $datas = $user->where('id' ,$id)->value('password');
       // dd($datas);
       $user->exists = true;
       $user->id = $id;
       $old_password= $request->input('old_password');
       $password=$request->input('password');
       if( $old_password== $datas){
        $request->validate([
            'password'        =>      'required|min:6'
            
        ]);
           $user->password=$request->input('password');  
           $user->save(); 
           return back()->with('success', 'password  Updated.');
        }
       else{
           return back()->with('success', 'Enter correct password.');
        }
       //  dd()
    }  
    function user_order(Request $request)
    {
        $user =new AdminModel();
        $id_chcek= session("id");
       
        $data1=$request->input('d_p');
        $data2=$request->input('no');
        $data3=$request->input('ids');
        $multi=$data1*$data2;
        $add=addbyModel::where('u_id',$id_chcek)->value('ids');
        

        $code_stock=code_purchaseModel::where('sess_id',$add)->whereNull('SN')->where('ids',$data3)->get()->count();
        // dd($code_stock);
       
       
        $check=$user->where('id',$id_chcek)->value('sold');
        $user->exists = true;
        $user->id =$id_chcek;
       
      
        
        if($data2<=$code_stock)
        if($multi<=$check)
      
        { 
            $user->sold=$check-$multi;
            $user->save();
           
            $i=0;
            $data4=$request->input('no');
            while($i<=$data4)
            // for($i=0; $i<=$data2;$i++)
            {
                $codes=code_purchaseModel::where('sess_id',$add)->whereNull('SN')->where('ids',$data3)->take($i)->get();

                  
                $row=new user_code_purModel();
                foreach ($codes as $value) 
                {
                
                $cod=$value->code;
                $id= $value->id;
                $ids=$value->ids;
                $sess=$value->sess_id;
               
                $row->code = $cod;
                $row->id = $id;
                $row->ids = $ids;
                $row->sess_id=$id_chcek;
                $row->purchaser_id=$sess;
                $row->save();
                }
                
            $i++;
        }
            $code2=code_purchaseModel::where('sess_id',$add)->whereNull('SN')->where('ids',$data3)->take($data2)->get();
            $code3=code_purchaseModel::where('sess_id',$add)->whereNull('SN')->where('ids',$data3)->take($data2)->delete();
            return back()->with(compact('code2'))->with('success', "you Purchased a codes");
        }

        
       
        else{
            return back()->with('success', "Sorry You can't have sufficient sold.");
        }
    else{
        return back()->with('success', "Sorry Code are out of stock");

    }



        

    }




    
}
