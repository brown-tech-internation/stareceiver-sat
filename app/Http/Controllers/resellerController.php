<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use App\Model\AdminModel;
use App\Model\soldModel;
use App\Model\soldhistroyModel;
use App\Model\catModel;
use App\Model\sub_catModel;
use App\Model\addbyModel;
use App\Model\packModel;
use App\Model\packageModel;
use App\Model\package_resellerModel;
use App\Model\pack_reseller;
use App\Model\send_reseller_packModel;
use App\Model\advertisementModel;
use App\Model\codesave;
use App\Model\code_purchaseModel;
use App\Model\code_purchase_history;

use DB;

class resellerController extends Controller
{
   
  
    function re_index()
    {
       
       
        $data = session("email");
        $id = session("id");
        $user =new AdminModel();
        $datas = $user->where('id' ,$id)->get();
        $add=advertisementModel::get();     
    
        $use1 = catModel::where('id',1)->get();
        $DONGEL=DB::table('sub_cat')
        ->join('cat','sub_cat.id', '=','cat.id')
        ->join('pack','sub_cat.ids', '=','pack.ids')
        ->join('package','pack.p_id', '=','package.p_id')
        ->join('send_pack','package.p_id', '=','send_pack.p_id')
        ->where('send_pack.id',$id)
        ->where('cat.cat','=','DONGEL')
        ->get()->sum('pk_price');
        $use2 = catModel::where('id',2)->get();
        $SERVERS=DB::table('sub_cat')
        ->join('cat','sub_cat.id', '=','cat.id')
        ->join('pack','sub_cat.ids', '=','pack.ids')
        ->join('package','pack.p_id', '=','package.p_id')
        ->join('send_pack','package.p_id', '=','send_pack.p_id')
        ->where('send_pack.id',$id)
        ->where('cat.cat','=','SERVERS')
        ->get()->sum('pk_price');
        $use3 = catModel::where('id',3)->get();
        $IPT=DB::table('sub_cat')
        ->join('cat','sub_cat.id', '=','cat.id')
        ->join('pack','sub_cat.ids', '=','pack.ids')
        ->join('package','pack.p_id', '=','package.p_id')
        ->join('send_pack','package.p_id', '=','send_pack.p_id')
        ->where('send_pack.id',$id)
        ->where('cat.cat','=','IPTV')
        ->get()->sum('pk_price');
        $use4 = catModel::where('id',4)->get();
        $VOD=DB::table('sub_cat')
        ->join('cat','sub_cat.id', '=','cat.id')
        ->join('pack','sub_cat.ids', '=','pack.ids')
        ->join('package','pack.p_id', '=','package.p_id')
        ->join('send_pack','package.p_id', '=','send_pack.p_id')
        ->where('send_pack.id',$id)
        ->where('cat.cat','=','VOD')
        ->get()->sum('pk_price');
        $use5 = catModel::where('id',5)->get();
        $TEST=DB::table('sub_cat')
        ->join('cat','sub_cat.id', '=','cat.id')
        ->join('pack','sub_cat.ids', '=','pack.ids')
        ->join('package','pack.p_id', '=','package.p_id')
        ->join('send_pack','package.p_id', '=','send_pack.p_id')
        ->where('send_pack.id',$id)
        ->where('cat.cat','=','TEST')
        ->get()->sum('pk_price');
        $use6 = catModel::where('id',6)->get();
        $GIFTCARDS=DB::table('sub_cat')
        ->join('cat','sub_cat.id', '=','cat.id')
        ->join('pack','sub_cat.ids', '=','pack.ids')
        ->join('package','pack.p_id', '=','package.p_id')
        ->join('send_pack','package.p_id', '=','send_pack.p_id')
        ->where('send_pack.id',$id)
        ->where('cat.cat','=','GIFT CARDS')
        ->get()->sum('pk_price');
        $use7 = catModel::where('id',7)->get();
        $BEINSPORTS=DB::table('sub_cat')
        ->join('cat','sub_cat.id', '=','cat.id')
        ->join('pack','sub_cat.ids', '=','pack.ids')
        ->join('package','pack.p_id', '=','package.p_id')
        ->join('send_pack','package.p_id', '=','send_pack.p_id')
        ->where('send_pack.id',$id)
        ->where('cat.cat','=','BEIN SPORTS')
        ->get()->sum('pk_price');
        $use8 = catModel::where('id',8)->get();
        $NETFLIXACCOUNTS=DB::table('sub_cat')
        ->join('cat','sub_cat.id', '=','cat.id')
        ->join('pack','sub_cat.ids', '=','pack.ids')
        ->join('package','pack.p_id', '=','package.p_id')
        ->join('send_pack','package.p_id', '=','send_pack.p_id')
        ->where('send_pack.id',$id)
        ->where('cat.cat','=','NETFLIX ACCOUNTS')
        ->get()->sum('pk_price');
        $use9 = catModel::where('id',9)->get(); 
        $MOBILEINTERNET=DB::table('sub_cat')
        ->join('cat','sub_cat.id', '=','cat.id')
        ->join('pack','sub_cat.ids', '=','pack.ids')
        ->join('package','pack.p_id', '=','package.p_id')
        ->join('send_pack','package.p_id', '=','send_pack.p_id')
        ->where('send_pack.id',$id)
        ->where('cat.cat','=','MOBILE & INTERNET')
        ->get()->sum('pk_price');
    //    dd($MOBILEINTERNET,$NETFLIXACCOUNTS,$GIFTCARDS,$IPT);
        return view('reseller/re_index',  compact('data' ,'datas','use1','use2','use3','use4','use5','use6','use7','use8','use9','DONGEL','SERVERS','IPT','VOD','TEST','GIFTCARDS','BEINSPORTS','NETFLIXACCOUNTS','MOBILEINTERNET','add')) ;
    }
    function sub_cat_reseller($id){
        $data = session("email");
        $ids = session("id");
        $user =new AdminModel();
        $datas = $user->where('id' ,$ids)->get();
       
        $users =DB::table('send_pack')
        ->join('package','send_pack.p_id', '=','package.p_id')
        ->join('pack','package.p_id', '=','pack.p_id')
        ->join('sub_cat','pack.ids', '=','sub_cat.ids')
        ->join('cat','sub_cat.id', '=','cat.id')
        ->where('send_pack.id',$ids)
        ->where('sub_cat.id',$id)
        ->get();
        // dd($users);
        return view('reseller/sub_cat_resell',  compact('data' ,'datas','users')) ;

    }
    public function code()
    {
        $data = session("email");
        $id = session("id");
        $user =new AdminModel();
        $datas = $user->where('id' ,$id)->get();
        $codes=DB::table('package')
        ->join('send_pack','package.p_id', '=','send_pack.p_id')
        ->join('pack','package.p_id', '=','pack.p_id')
        ->join('sub_cat','pack.ids', '=','sub_cat.ids')
        ->join('code_purchase','sub_cat.ids', '=','code_purchase.ids')
        ->where('send_pack.id',$id)
        ->where('code_purchase.sess_id',$id)->orderBy('cp_id', 'DESC')
        ->paginate(15);
        // dd($codes);
    

        // $car=Carbon::yesterday()->format('Y-m-d');
        // $countYesterday = code_purchaseModel::where('creatf' ,'>=', Carbon::yesterday())->get();
        // $code=code_purchaseModel::whereDay('creatf', now()->day)->get();
         
        // dd($countYester,$countYesterday,$car);
        return view('reseller/code', compact('data' ,'datas','codes'));
    }
    function reseller_update_code(Request $request,$cp_id){
        
        $use=code_purchaseModel::findOrFail($cp_id);
        $use->SN=$request->input('cn');
        $use->clint_name=$request->input('clint_name');
        $use->save();
        return back()->with('success', 'Code Update');


    }
    public function search_code()
    {
    $data = session("email");
    $id = session("id");
    $user =new AdminModel();
    $datas = $user->where('id' ,$id)->get();
    $sub=DB::table('sub_cat')->get(); 
    $today=Carbon::today()->format('Y-m-d');

    
    return view('reseller/search_code', compact('data','datas','sub','today'));
    }
    function statics()
    {
        $data = session("email");
        $id = session("id");
        $user =new AdminModel();
        $datas = $user->where('id' ,$id)->get();
        $today=Carbon::today()->format('Y-m-d');
        return view('reseller/statics', compact('data','datas','today','id'));

    }
    function statics_post(Request $request){
        $data = session("email");
        $id = session("id");
        $user =new AdminModel();
        $datas = $user->where('id' ,$id)->get();
        $today=Carbon::today()->format('Y-m-d');
        return view('reseller/statics' ,compact('data','datas','today','request','id'));
    }
    function serch(Request $request )
    { 
      
      $from=$request->input('from');
      $to=$request->input('to');
      $sv=$request->input('sv');
      $sc=$request->input('sc');

     

      $ids=$request->input('ids');
   if ($ids==null) {
        $user =DB::table('code_purchase')
       
        ->get();
    }
    else{
      $user =DB::table('code_purchase')
      ->where('ids','LIKE' ,'%'.$ids.'%')
      ->whereBetween('creatf', [$from,$to])
      
      ->get();
    }  
    //   dd($user);
     if (!$user || !$user->count()) {
        Session::flash('no-results', 'Your search produced no results');
    }
    return view ('reseller/search_code',['data' => $user]);

    }
    public function send()
    {
        $data = session("email"); 
        $id = session("id");
        $user =new AdminModel();
        $datas = $user->where('id' ,$id)->get(); 
        $useres =new soldModel();
        $dataes = $useres->where('addby' ,$id)->get();  
    return view('reseller/send', compact('data','datas','dataes'));
    }
    public function receive()
    {
        $data = session("email");
        $id = session("id");
        $user =new AdminModel();
        $datas = $user->where('id' ,$id)->get();
        $user1=new soldhistroyModel();
        $data1 = $user1->where('addby' ,$id)->orderBy('ids', 'DESC')->take(20)->get();

    return view('reseller/receive', compact('data','datas','data1'));
    }
    public function demond()
    {
        $data = session("email");
        $datass = session("id");
        $id = session("id");

        $user =new AdminModel();
        $datas = $user->where('id' ,$id)->get();
        
        $user1=new soldhistroyModel();
        $data1 = $user1->where('id' ,$id)->orderBy('ids', 'DESC')->get();
       
    return view('reseller/demond', compact('data','datass','datas','data1'));
    }
    public function add_user()
    {
        $data = session("email");
        $id = session("id");
        $user =new AdminModel();
        $datas = $user->where('id' ,$id)->get();
    return view('reseller/add_user', compact('data','datas'));
    }
    public function all_resellers()
    {
        $data = session("email");
        $id = session("id");
        $user =new AdminModel();
        $datas = $user->where('id' ,$id)->get();


        $users =DB::table('addby')
        ->join('users','addby.email', '=','users.email')
        ->where('ids' ,$id)
        ->get();
    

    return view('reseller/all_resellers', compact('data','datas','users'));
    }
    public function today()
    {
        $data = session("email");
        $id = session("id");
        $user =new AdminModel();
        $datas = $user->where('id' ,$id)->get();
    return view('reseller/today', compact('data','datas','id'));
    }
     public function yesterday()
    {
        $data = session("email");
        $id = session("id");
        $user =new AdminModel();
        $datas = $user->where('id' ,$id)->get();
    return view('reseller/yesterday' , compact('data','datas','id'));
    }
    public function charging()
    {
        $data = session("email");
        $id = session("id");
        $user =new AdminModel();
        $datas = $user->where('id' ,$id)->get();
       
    return view('reseller/charging' , compact('data','datas'));
    }    
    public function ressller_pack()
    {
        $data = session("email");
        $id = session("id");
        $user =new AdminModel();
        $datas = $user->where('id' ,$id)->get();
        $pac =new package_resellerModel();
        $pack=$pac->where('sess_id',$id)->get();
        // dd($pack);
    return view('reseller/ressller_pack' , compact('data','datas','pack'));
    } 
    function see_reseller_pack($pr_id){
        
        $data = session("email");
        $ids   = session("id");
        $user =new AdminModel();
        $datas = $user->where('id' ,$ids)->get();
        $packs =DB::table('package_reseller')
        ->join('pack_reseller','package_reseller.pr_id', '=','pack_reseller.pr_id')
        ->join('sub_cat','pack_reseller.ids', '=','sub_cat.ids')
        ->where('pack_reseller.pr_id',$pr_id)
        ->get();
        // dd($packs);
        return view('reseller/see_reseller_pack_info',compact('data','datas','packs'));
    }
    function reseller_package_update(Request $request,$pr_id)
    {
        $use=package_resellerModel::findOrFail($pr_id);
        $use->p_name=$request->input('p_name');
        $use->save();
        $user = pack_reseller::findOrFail($pr_id);
        $user->delete();
        for( $i=0; $i<count($request->input('ids'));$i++)
        {
            $user = new pack_reseller();
            $user->pr_id = $request->pr_id; 
            $user->p_id = $request->p_id; 
            $user->r_pk_name = $request->p_name; 
            $user->ids = $request->ids[$i];
            $user->r_pk_price = $request->pri[$i];
            $user->save(); 
        }
    return back()->with('success', ' Package Updated.');

    }
    public function add_package()
    { $data = session("email");
        $id = session("id");
        $user =new AdminModel();
        $datas = $user->where('id' ,$id)->get();
        $users =DB::table('send_pack')
        ->join('package','send_pack.p_id', '=','package.p_id')
        ->join('pack','package.p_id', '=','pack.p_id')
        ->join('sub_cat','pack.ids', '=','sub_cat.ids')
        ->join('cat','sub_cat.id', '=','cat.id')
        ->where('send_pack.id',$id)
        ->get();
        // dd($users);

    return view('reseller/add_package', compact('data','datas','users','id'));
    }
    function reseller_package(Request $request)
    {
    //     $p_name=$request->input('p_name');
    //    $ids = $request->input('ids');
    //    $pk_price= $request->input('pri');
    //    dd($p_name,$ids,$pk_price);
            $request->validate([
                'p_name'        =>      'required|unique:package_reseller'
            ]);

        $use  =new package_resellerModel();
        $use->sess_id=$request->input('sess_id');
        $use->p_name=$request->input('p_name');
        $use->p_id=$request->input('p_id');
        $use->save();

        $check_name = $request->input('p_name');
        $use  = new package_resellerModel();
        $pr_id=$use->where('p_name',$check_name)->value('pr_id');
       
      
       
        for( $i=0; $i<count($request->input('ids'));$i++)
        {
            
       $user = new pack_reseller();
       $user->pr_id=$pr_id;
       $user->p_id=$request->input('p_id');
       $user->r_pk_name=$request->input('p_name');
       $user->ids = $request->input('ids')[$i];
       $user->r_pk_price= $request->input('pri')[$i];
      
       $user->save(); 
        }
        
        
           return back()->with('success', 'Package Created.');
    }
    // public function ressller_pack()
    
    public function change_price()
    {
        $data = session("email");
        $id = session("id");
        $user =new AdminModel();
        $datas = $user->where('id' ,$id)->get();
    return view('reseller/change_price' , compact('data','datas'));
    }
    public function change_code()
    {
        $data = session("email");
        $id = session("id");
        $user =new AdminModel();
        $datas = $user->where('id' ,$id)->get();
    return view('reseller/change_code' , compact('data','datas'));
    }
    public function being_sport()
    {
        $data = session("email");
        $id = session("id");
        $user =new AdminModel();
        $datas = $user->where('id' ,$id)->get();
    return view('reseller/being-sport', compact('data','datas'));
    }
    public function prices()
    {
        $data = session("email");
        $id = session("id");
        $user =new AdminModel();
        $datas = $user->where('id' ,$id)->get();
        $pack=DB::table('send_pack')
        ->join('pack','send_pack.p_id', '=','pack.p_id')
        ->join('sub_cat','pack.ids', '=','sub_cat.ids')
        ->where('send_pack.id',$id)
        ->get();
    return view('reseller/prices' , compact('data','datas','pack'));
    }
    function save_user(Request $request)
    {
        
        $request->validate([
            'email'        =>      'required',
            'password'         =>      'required|min:3',
            'mobile'             =>      'required|min:6'
            
        ]);
       $user = new AdminModel();
       $user->email=$request->input('username');
      
       $user->password = $request->input('password');
       $user->phone= $request->input('mobile');
       $user->usertype= $request->input('usertype');
       $user->save();
       $u_id=AdminModel::orderby('id', 'desc')->value('id');
       $id = session("id");
       $mail = session("email");
       $use=new addbyModel();
       $use->u_id=$u_id;
       $use->ids=$id;
       $use->addby=$mail;
       $use->email=$request->input('username');
       $use->save();
       
       
       if(!is_null($user)) {
        return back()->with('success', 'Your reseller successfully added.');
        }
        else {
        return back()->with('error', 'Whoops! some error encountered. Please try again.');
        }  
    }
    function r_reslller_info($ids){
        $data = session("email");
        $id = session("id");
        $user =new AdminModel();
        $datas = $user->where('id' ,$id)->get();
        $pac=new package_resellerModel();
        $pack=$pac->where('sess_id',$id)->get();
       
        $datae=$user->where('id',$ids)->get();
       
        return view('reseller/r_reslller_info',compact('data','datas','datae','pack'));
    }
    function r_reseller_update(Request $request, $id)
    {   
        $user =new AdminModel();
        $user->exists = true;
        $user->id =$id;
        
        $user->email=$request->input('username');
        $user->password = $request->input('password');
        $user->phone= $request->input('mobile');
        $user->save();

        $datas=$user->where('id',$id)->value('id');
      
        $use=new addbyModel();
        $use->exists = true;
        $use->u_id =$datas;
        $use->email=$request->input('username');
        $use->save();
        
        if(!is_null($user)) {
         return back()->with('success', 'Your reseller successfully updated.');
         }
         else {
         return back()->with('error', 'Whoops! some error encountered. Please try again.');
         }
    }
    function reseller_send_sold(Request $request)
    {
        $request->validate([
            'sold'         =>      'required'
            
        ]);
       $reselller_name = $request->input('reselller_name');
       $sold = $request->input('sold');

       $users = new AdminModel();

      
       $datas=$users->where('id',$reselller_name)->value('sold');
       $users->exists = true;
       $users->id = $reselller_name;
       
    //    dd($datas,$sold);

       $users->sold=($request->input('sold')+$datas);
      
       
       $users->save();
       if(!is_null($users)) {
        return back()->with('success', 'Your sold successfully added.');
        }
        else {
        return back()->with('error', 'Whoops! some error encountered. Please try again.');
        }   	         
    }
    function reseller_send_pack(Request $request)
    {
        $sess_id = session("id");

        $user=new send_reseller_packModel();
        $user->id=$request->input('id');
        $user->sess_id=$sess_id;
        $user->pr_id=$request->input('pr_id');
        $user->save();
        $id=$request->input('id');  
        $users = send_reseller_packModel::findOrfail($id);
        $users->delete();

        $user=new send_reseller_packModel();
        $user->id=$request->input('id');
        $user->sess_id=$sess_id;
        $user->pr_id=$request->input('pr_id');
       
        $user->save();          
        if(!is_null($user)) {
         return back()->with('success', 'Your package successfully added.');
         }
         else {
         return back()->with('error', 'Whoops! some error encountered. Please try again.');
         }   
    }
    function reseller_order(Request $request)
    {
        
       
        $data1=$request->input('d_p');
        $data2=$request->input('no');
        $data3=$request->input('ids');
        $multi=$data1*$data2;
        

        $code_stock=codesave::where('ids',$data3)->get()->count();
        
        


        
        $user =new AdminModel();
        $id_chcek= session("id");
       
        $check=$user->where('id',$id_chcek)->value('sold');
        $user->exists = true;
        $user->id =$id_chcek;
       
      
        if($data2<=$code_stock)
            if($multi<=$check)
          
            { 
                $user->sold=$check-$multi;
                $user->save();
               
                $i=0;
                $data4=$request->input('no');
                while($i<=$data4)
                // for($i=0; $i<=$data2;$i++)
                {
                    $codes=codesave::where('ids',$data3)
                    ->take($i)->get();

                    //    dd($code);
                    $row=new code_purchaseModel();
                    
                    foreach ($codes as $value) 
                    {
                    
                   


                   
                    $cod=$value->code;
                    $id= $value->id;
                    $ids=$value->ids;
                   
                    $row->code = $cod;
                    $row->id = $id;
                    $row->ids = $ids;
                    $row->sess_id=$id_chcek;
                    $row->save();
                }
                $i++;
            }
                  
            
            for($j=0; $j<=$data2;$j++)
            {        
                $coder=codesave::where('ids',$data3)
                    ->take($j)->get(); 
                $his=new code_purchase_history();
                foreach ($coder as $value) 
                    {
                    
                   


                    $cod1=$value->code;
                    $id1= $value->id;
                    $ids1=$value->ids;
                
                    $his->code = $cod1;
                    $his->id = $id1;
                    $his->ids = $ids1;
                    $his->sess_id=$id_chcek;
                    $his->save();
               
                    }   
                }        
             
                $code2=codesave::where('ids',$data3)->take($data2)->delete();
                return back()->with('success', "you Purchased a codes");
            }

            else{
                return back()->with('success', "Sorry You can't have sufficient sold.");
            }
        else{
            return back()->with('success', "Sorry Code are out of stock");

        }

    }
    function demand_sold(Request $request)
    {
        
        $request->validate([
            'name'        =>      'required',
            'demand'         =>      'required'
            
            
        ]);
       $id = session("id");
       $addby= new addbyModel();
       $add=$addby->where('u_id',$id)->value('ids');
       $user = new soldModel();
       $users = new soldhistroyModel();
       $user->email=$request->input('name');
       $user->id=$request->input('id');
       $user->soldss= $request->input('demand');
       $user->addby= $add;



       $users->email=$request->input('name');
       $users->id=$request->input('id');
       $users->solds= $request->input('demand');
       $users->addby= $add;
      


       $users->save();
       $user->save();

       if(!is_null($user)) {
        return back()->with('success', 'Your Request successfully send.');
        }
        else {
        return back()->with('error', 'Whoops! some error encountered. Please try again.');
        }  
    } 
    function approves(Request $request,$id,$ids,$idd){
       
        $user =new AdminModel();
        $datas=$user->where('id',$id)->value('sold');
        $user->exists = true;
        $user->id = $id;
        $idminus= session("id");
        $datass=$request->input('sold');
        $check=$user->where('id',$idminus)->value('sold');

        // dd($check,$datass);
       
     //    dd($datas);
       
       
         if($datass<=$check)
         {
         $user->sold=($request->input('sold')+$datas);
         $user->save();
         } else{
            return back()->with('success', "Sorry You can't have sufficient sold.");
         }
         $users =new AdminModel();
        //  $idminus= session("id");
         $datas=$users->where('id',$idminus)->value('sold');
         $users->exists = true;
         $users->id = $ids;
         $users->sold=($datas-$request->input('sold'));
        
        
         $users->save();
        // dd($user,$users);
 
         DB::table('sold')->where('ids', $idd)->delete();
         $users=new soldhistroyModel();
         $datas=$user->where('id',$id);
         $users->exists = true;
         $users->id = $id;
         $users->state=1;
         $users->save();
         if(!is_null($users)) {
         return back()->with('success', 'Sold Successfully  Approved.');
         }
         else {
         return back()->with('error', 'Whoops! some error encountered. Please try again.');
         }
 
     } 
     function setting()
     {
        $data = session("email");
        $id = session("id");
        $user =new AdminModel();
        $datas = $user->where('id' ,$id)->get();
        return view('reseller/setting', compact('data','datas'));
     }  
     function password_update(Request $request)
     {
        $id = session("id");
        $user =new AdminModel();
       
       
        $datas = $user->where('id' ,$id)->value('password');
        // dd($datas);
        $user->exists = true;
        $user->id = $id;
        $old_password= $request->input('old_password');
        $password=$request->input('password');
        if( $old_password== $datas){
            $request->validate([
                'password'        =>      'required|min:6'
                
            ]);
            $user->password=$request->input('password');  
            $user->save(); 
            return back()->with('success', 'password  Updated.');
         }
        else{
            return back()->with('success', 'Enter correct password.');
         }
        //  dd()
     }  	 
         
}
