<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Category;
use App\Model\AdminModel;
use App\Model\codesave;
use App\Model\add_reseller;
use App\Model\add_charging;
use App\Model\admin_soldModel;
use App\Model\catModel;
use App\Model\sub_catModel;
use App\Model\soldModel;
use App\Model\soldhistroyModel;
use App\Model\packageModel;
use App\Model\packModel;
use App\Model\addbyModel;
use App\Model\send_packModel;
use App\Model\advertisementModel;
use App\Model\admin_code_purModel;
use DB;
use Carbon\Carbon;


class adminController extends Controller
{
    public function sub_cat_code(Request $request)
    {
        $states = DB::table("sub_cat")
        ->where("id",$request->country_id)
        ->pluck("name","ids");
        return response()->json($states);
    }
    function static(){
        $user =new AdminModel();
        $data = $user->get();
        $today=Carbon::today()->format('Y-m-d');

        return view('wp-admin/static' ,compact('data','today'));
    }

    function static_post(Request $request){
        $user =new AdminModel();
        $data = $user->get();
        $today=Carbon::today()->format('Y-m-d');
        return view('wp-admin/static' ,compact('data','today','request'));
    }
    public function admin()
    {    
        $use1 = catModel::where('id',1)->get();
        $use2 = catModel::where('id',2)->get();
        $use3 = catModel::where('id',3)->get();
        $use4 = catModel::where('id',4)->get();
        $use5 = catModel::where('id',5)->get();
        $use6 = catModel::where('id',6)->get();
        $use7 = catModel::where('id',7)->get();
        $use8 = catModel::where('id',8)->get();
        $use9 = catModel::where('id',9)->get();
        $add=advertisementModel::get();
        
    
        $user =new codesave();
        $data = $user->all();  
        return view('wp-admin/main',compact('use1','use2','use3','use4','use5','use6','use7','use8','use9','data','add'));
        
    }
    function admin_sub_cat($id){
        $user =new AdminModel();
        $data = $user->get();
        $users =DB::table('sub_cat')
        ->join('cat','sub_cat.id', '=','cat.id')
        ->where('sub_cat.id',$id)
        ->get();
        return view('wp-admin/admin_sub_cat', compact('data','users'));
    }
    
    function code()
    {
        $user =new AdminModel();
        $data = $user->get();
        $datae =DB::table('admin_code_pur')
        ->join('sub_cat','admin_code_pur.ids', '=','sub_cat.ids')->orderby('ad_id', 'desc')->get();
        return view('wp-admin/code', compact('data','datae'));
    }
    function code_purchase_his()
    {
        $user =new AdminModel();
        $data = $user->get();
        $datae =DB::table('code_purchase_his')
        ->join('sub_cat','code_purchase_his.ids', '=','sub_cat.ids')
        ->join('users','code_purchase_his.sess_id', '=','users.id')
        ->orderby('chp_id', 'desc')->get();
        // dd($datae);
        return view('wp-admin/code_purchase_his', compact('data','datae'));
    }

    function code_dels($Nid)
    {
        $user = codesave::findOrFail($Nid);
       
        $user->delete();
        return back()->with('success','Code Delete');
       
    }
    function admin_update_code(Request $request,$ad_id){
        
        $use=admin_code_purModel::findOrFail($ad_id);
        $use->SN=$request->input('cn');
        $use->clint_name=$request->input('clint_name');
        $use->save();
        return back()->with('success', 'Code Update');


    }
    function sold()
    {
        $user =new AdminModel();
        $data = $user->get();

        
      
      
       
       $dataes=DB::table('sold')
       ->join('users','sold.id', '=','users.id')->where('usertype','reseller')->get();
    //    
//   dd($dataes);


        return view('wp-admin/sold', compact('data','dataes'));
       
    }
    function sold_his($id)
    {
        $user =new AdminModel();
        $data = $user->get();
        $dataes=DB::table('sold_history')->where('id',$id)->orderby('ids', 'desc')->get();
       return view('wp-admin/sold_his', compact('data','dataes'));
    }
    function admin_order_code(Request $request){
        // 



        
        $data2=$request->input('no');
        $data3=$request->input('ids');
       
        

        $code_stock=codesave::where('ids',$data3)->get()->count();
    
        if($data2<=$code_stock)
          
            { 
               
                $i=0;
                $data4=$request->input('no');
                while($i<=$data4)
                // for($i=0; $i<=$data2;$i++)
                {
                    $codes=codesave::where('ids',$data3)
                    ->take($i)->get();

                    //    dd($code);
                    $row=new admin_code_purModel();
                    
                    foreach ($codes as $value) 
                    {
                    $cod=$value->code;
                    $id= $value->id;
                    $ids=$value->ids;
                    $row->code = $cod;
                    $row->id = $id;
                    $row->ids = $ids;
                    $row->save();
                }
                $i++;
                  
            }
              
             
                $code2=codesave::where('ids',$data3)->take($data2)->delete();
                return back()->with('success', "you Purchased a codes");
            }

          
        else{
            return back()->with('success', "Sorry Code are out of stock");

        }
        
    }
    
    function add_seller()
    {
        $user =new AdminModel();
        $data = $user->get();
        return view('wp-admin/add-seller', compact('data'));
    }
    function reseller_save(Request $request)
    {
        $request->validate([
            'username'        =>      'required',
            'password'         =>      'required|min:3',
            'mobile'             =>      'required|min:6'
            
            
        ]);
        $user = new AdminModel();
        $user->email=$request->input('username');
        $user->password = $request->input('password');
        $user->phone= $request->input('mobile');
        $user->usertype= $request->input('usertype');
        $user->save();
        $u_id=AdminModel::orderby('id', 'desc')->value('id');
      
        $id = session("id");
        $mail = session("email");
        $use=new addbyModel();
        $use->u_id=$u_id;
        $use->ids=$id;
        $use->addby=$mail;
        $use->email=$request->input('username');
        $use->save();
       
        if(!is_null($user)) {
            return back()->with('success', 'Your reseller successfully added.');
        }
        else {
            return back()->with('error', 'Whoops! some error encountered. Please try again.');
        }   	 
    }

    function reseller()
    {
        $user =new AdminModel();
        $data = $user->get();
        $users =DB::table('addby')
        ->join('users','addby.email', '=','users.email')
        ->get();
        return view('/wp-admin/reseller',compact('data','users')) ;
    }
    function admin_reseller()
    {   $id = session("id");
        $user =new AdminModel();
        $data = $user->get();
        $users =DB::table('addby')
        ->join('users','addby.email', '=','users.email')
        ->where('ids',$id)
        ->get();
        return view('/wp-admin/admin_reseller',compact('data','users')) ;
    }
    function reslller_info($id){
        $user =new AdminModel();
        $data = $user->get();
        $datas=$user->where('id',$id)->get();
        $pack=new packageModel();
        $pack=$pack->all();
        return view('/wp-admin/reslller_info',compact('data','datas','pack'));
    }
    function reseller_update(Request $request, $id)
    {   
        $user =new AdminModel();
        $user->exists = true;
        $user->id =$id;
        
        $user->email=$request->input('username');
        $user->password = $request->input('password');
        $user->phone= $request->input('mobile');
        $user->save();

        $datas=$user->where('id',$id)->value('id');
      
        $use=new addbyModel();
        $use->exists = true;
        $use->u_id =$datas;
        $use->email=$request->input('username');
        $use->save();
        
        if(!is_null($user)) {
         return back()->with('success', 'Your reseller successfully updated.');
         }
         else {
         return back()->with('error', 'Whoops! some error encountered. Please try again.');
         }
    }
    function disactive($id)
    {
        $user =new AdminModel();
        $user->exists = true;
        $user->id =$id;
        $user->state=1;
        $user->save();
        if(!is_null($user)) {
        return back()->with('success', 'This user successfully Deactivate.');
        }
        else {
        return back()->with('error', 'Whoops! some error encountered. Please try again.');
            } 

    }
    function active($id)
    {
        $user =new AdminModel();
        $user->exists = true;
        $user->id =$id;
        $user->state=0;
        $user->save();
        if(!is_null($user)) {
            return back()->with('success', 'This user successfully activate.');
            }
            else {
            return back()->with('error', 'Whoops! some error encountered. Please try again.');
                }         

    }
    function delete_user($id)
    {
        DB::table('users')->where('id', $id)->delete();
        return back()->with('success', 'This user successfully Delete.');
  
    }
    function admi_send_sold(Request $request){
        $request->validate([
            'sold'         =>      'required'
            
        ]);

       $reselller_name = $request->input('reselller_name');
       $sold =$request->input('sold');
       $users = new AdminModel();


      
       $datas=$users->where('id',$reselller_name)->value('sold');
       $users->exists = true;
       $users->id = $reselller_name;
       
    //    dd($datas,$sold);

       $users->sold=($request->input('sold')+$datas);
      
       
       $users->save();
       if(!is_null($users)) {
        return back()->with('success', 'Your sold successfully added.');
        }
        else {
        return back()->with('error', 'Whoops! some error encountered. Please try again.');
        }   	

    }
    function admin_send_pack(Request $request){
        
        $user=new send_packModel();
        $user->id=$request->input('user_id');
        $user->p_id=$request->input('p_id');
        $user->save();
        $user_id=$request->input('user_id');  
        $users = send_packModel::findOrfail($user_id);
        $users->delete();

        $user=new send_packModel();
        $user->id=$request->input('user_id');
        $user->p_id=$request->input('p_id');
       
        $user->save();          
        if(!is_null($user)) {
         return back()->with('success', 'Your package successfully added.');
         }
         else {
         return back()->with('error', 'Whoops! some error encountered. Please try again.');
         }   	


    }
    function today()
    {
        $user =new AdminModel();
        $data = $user->get();
      
        return view('wp-admin/today', compact('data'));
    }
    function yesterday()
    {
        $user =new AdminModel();
        $data = $user->get();
        return view('wp-admin/yesterday', compact('data'));
    }
    function charging()
    {
        $user =new AdminModel();
        $data = $user->get();
        return view('wp-admin/charging', compact('data'));
    }
    function packages()
    {
        $user =new AdminModel();
        $data = $user->get();
        return view('wp-admin/packages', compact('data'));
    }
    function begin_sport()
    {
        $user =new AdminModel();
        $data = $user->get();
        return view('wp-admin/begin-sport', compact('data'));

    }
    function change_prices()
    {
        $user =new AdminModel();
        $data = $user->get();
        return view('wp-admin/change-prices', compact('data'));
    }
    function add_codes()
    {
        
        $user =new AdminModel();
       
        $data = $user->get();
        $datas = DB::table('cat')->get();

        return view('wp-admin/add-codes',compact('data','datas'));
        
    }
    public function sub($id){
        echo json_encode(DB::table('sub_cat')->where('id', $id)->get());
    }

    function add_cat(){
        $user=new catModel();
        $data=$user->all();
        
        $datas=DB::table('cat')
        ->join('sub_cat','cat.id', '=','sub_cat.id')
        ->get();
            
        
        return view('wp-admin/add_cat',['data' => $data , 'datas'=>$datas]);
    }
  
    function sub_cat_bs(Request $request, $id)
    {
        $user =new sub_catModel();
        $data = $user->where('id' ,$id)->get();
        return view('wp-admin/sub_cat/sub_cat_bs',['data' => $data]);
    }
    function sub_cat($id)
    {
       
        DB::table('sub_cat')->where('ids', $id)->delete();
        DB::table('pack')->where('ids', $id)->delete();
        return redirect()->route('add_cat')->with('successed','Data Delete');

       
    }
    function add_sub_cat(Request $request){
        $request->validate([
            'id'        =>      'required',
            'name'         =>      'required',
            'price'          =>       'required'
        ]);
       $user = new sub_catModel();
       $user->id=$request->input('id');
       $user->name = $request->input('name');
       $user->price = $request->input('price');
       
      
       
       $user->save();
       if(!is_null($user)) {
        return back()->with('success', 'Your Type successfully added.');
        }
        else {
        return back()->with('error', 'Whoops! some error encountered. Please try again.');
        }
    }
    function prices(){
        $user =new AdminModel();
        $data = $user->get();
        $users =new sub_catModel();
        $datae =$users->get();
        return view('wp-admin/prices', compact('data','datae'));

    }
    function forget_password(){
        return view('forget_password');
    }
    public function code_save(Request $request)
    {
    	 $request->validate([
                'code'        =>      'required'
                
            ]);
           $coder= explode("\r\n",$request->input('code'));
           
           for( $i=0; $i<count($coder);$i++)
           {

            $user = new codesave();
            $user->id=$request->input('id');
            $user->ids=$request->input('ids');
            $user->code = $coder[$i]; 
            $user->save();
            }
           if(!is_null($user)) {
            return back()->with('success', 'You code successfully send.');
            }
            else {
            return back()->with('error', 'Whoops! some error encountered. Please try again.');
            }   	 
           
    }
    function add_charging(Request $request)
    {
        $request->validate([
            'serial_no'        =>      'required',
            'card_no'         =>      'required',
            'query'             =>      'required'
            
        ]);
       $user = new add_charging();
       $user->serial_no=$request->input('serial_no');
       $user->card_no = $request->input('card_no');
       $user->cat= $request->input('cat');
       $user->query= $request->input('query');
       
       $user->save();
       if(!is_null($user)) {
        return back()->with('success', 'Your Charge successfully added.');
        }
        else {
        return back()->with('error', 'Whoops! some error encountered. Please try again.');
        }   	 
           
    }
    function admin_send_sold(Request $request)
    {
        $request->validate([
            'reselller_name'        =>      'required',
            'sold'         =>      'required'
            
        ]);
       $reselller_name = $request->input('reselller_name');
       $sold = $request->input('sold');
       $users = new AdminModel();
      

      
       $datas=$users->where('id',$reselller_name)->value('sold');
       $name=$users->where('id',$reselller_name)->value('email');
       
       $users->exists = true;
       $users->id = $reselller_name;
       
    //    dd($datas,$sold);

       $users->sold=($request->input('sold')+$datas);
      
       
       $users->save();
       $history = new soldhistroyModel();
       $history->email=$name;
       $history->solds=$request->input('sold');
       $history->state=1;
       $history->id=$reselller_name;
       $history->save();




       if(!is_null($users)) {
        return back()->with('success', 'Your sold successfully added.');
        }
        else {
        return back()->with('error', 'Whoops! some error encountered. Please try again.');
        }   	         
    }
    function sold_req(){
        $user =new AdminModel();
        $data = $user->get();
        $dataes=DB::table('sold')
       ->join('users','sold.id', '=','users.id')->where('usertype','reseller')->get();
        return view('wp-admin/sold_req', compact('data','dataes'));
    }
    function sold_req_approve(Request $request,$id,$ids){
       
        $user =new AdminModel();
       $datas=$user->where('id',$id)->value('sold');
       $user->exists = true;
       $user->id = $id;
      
    //    dd($datas);
      
    //    $data->sold=$data+$request->input('sold');
        $user->sold=($request->input('sold')+$datas);
        $user->save();

        DB::table('sold')->where('ids', $ids)->delete();
        $users=new soldhistroyModel();
        $datas=$user->where('id',$id);
        $users->exists = true;
        $users->id = $id;
        $users->state=1;
        $users->save();
         
        
 
        if(!is_null($user)) {
         return back()->with('success', 'Sold Successfully  Paied.');
         }
         else {
         return back()->with('error', 'Whoops! some error encountered. Please try again.');
         }
 
     }

    function admin_setting()
    {
        $user =new AdminModel();
        $data = $user->get();
    
        $subcat=sub_catModel::where('id','8')->get();
        $subcat2=sub_catModel::where('id','!=','8')->get();
        $add=advertisementModel::get();
        $as=advertisementModel::value('show');
        // dd($add);
        return view('wp-admin/setting', compact('data','subcat','subcat2','add','as'));  
    }
    function advertisement(Request $request)
    {
        
        DB::table('advertisement')->delete();
        $ads= explode("\r\n",$request->input('ads'));
           
        for( $i=0; $i<count($ads);$i++)
        {

            $user=new advertisementModel();
            $user->ads= $ads[$i];
            $user->show = $request->input('show'); 
            $user->save();
        }       
        $user->save();
        if(!is_null($user)) {
            return back()->with('success', 'Your Add successfully added.');
        }
        else {
            return back()->with('error', 'Whoops! some error encountered. Please try again.');
        }   
    }
  
    function bein_sports(Request $req){
        
        
        $input = $req->all();
        $i = 0;
        $count = count($input['ids']);
        while($i < $count){
    
            $data[] = array(
                'ids' => $input['ids'][$i],
                'name' => $input['name'][$i],
                'price' => $input['pri'][$i],
                
            );
            $i++;
        }
        $j = 0;
        $count1 = count($input['ids']);
        while($j < $count1){
            DB::table('sub_cat')->where('ids',$data[$j]['ids'])->update($data[$j]);
            $j++;
        }
        return back()->with('success','BeIN Sport Prices Updated.');

    }
    function sub_cat_pri(Request $req){

        $input = $req->all();
        $i = 0;
        $count = count($input['ids']);
        while($i < $count){
    
            $data[] = array(
                'ids' => $input['ids'][$i],
                'name' => $input['name'][$i],
                'price' => $input['pri'][$i],
                
            );
            $i++;
        }
        $j = 0;
        $count1 = count($input['ids']);
        while($j < $count1){
            DB::table('sub_cat')->where('ids',$data[$j]['ids'])->update($data[$j]);
            $j++;
        }
    return back()->with('success','Prices Updated.');
        
    }
    function stock()
    {
       
        $user =new AdminModel();
       
        $data = $user->get();
        $datas = DB::table('sub_cat')->get();

        return view('wp-admin/stock',compact('data','datas'));
    }
    function code_stock($ids)
    {
        $user =new AdminModel();
       
        $data = $user->get();
        $datas = DB::table('code_save')->where('ids',$ids)->get();
        $pri = DB::table('sub_cat')->where('ids',$ids)->value('price');
        $name = DB::table('sub_cat')->where('ids',$ids)->value('name');

        return view('wp-admin/code_stock',compact('data','datas','pri','name'));
    }
    function stock_code_del($Nid)
    {
        $user = codesave::findOrFail($Nid);
       
        $user->delete();
        return back()->with('success','Code Delete');

    }
    function last200(){
        $user =new AdminModel();
       
        $data = $user->get();
        $datas =DB::table('code_save')
        ->join('sub_cat','code_save.ids', '=','sub_cat.ids')->take(200)->get();
        

        return view('wp-admin/last200',compact('data','datas'));

    }

    function password_update(Request $request)
    {
       $id = session("id");
       $user =new AdminModel();
      
      
       $datas = $user->where('id',$id)->value('password');
       // dd($datas);
       $user->exists = true;
       $user->id = $id;
       $old_password= $request->input('old_password');
       $password=$request->input('password');
       if( $old_password== $datas){
        $request->validate([
            'password'        =>      'required|min:6'
            
        ]);
           $user->password=$request->input('password');  
           $user->save(); 
           return back()->with('success', 'password  Updated.');
        }
       else{
           return back()->with('success', 'Enter correct password.');
        }
       //  dd()
    }  
    function add_new_packages(){
        
        $user =new AdminModel();
        $data = $user->get();
        $users =new sub_catModel();
        $datae =$users->get();
        return view('wp-admin/add_new_packages',compact('data','datae'));
    }
    function package(Request $request){

        $request->validate([
            'p_name'        =>      'required|unique:package'
            
            
        ]);
        
        $use  = new packageModel();
        $use->p_name=$request->input('p_name');
        $use->save();


        $check_name = $request->input('p_name');
        $use  = new packageModel();
        $data=$use->where('p_name',$check_name)->value('p_id');
      
        
        for( $i=0; $i<count($request->input('ids'));$i++)
        {
       $user = new packModel();
       $user->p_id=$data;
       $user->p_name=$request->input('p_name');
       $user->ids = $request->input('ids')[$i];
       $user->pk_price= $request->input('pri')[$i];
       
       $user->save(); 
        }
           return back()->with('success', 'Package Created.');
    //    dd($user);

    }
    function your_pack(){
       
        $user =new AdminModel();
        $data = $user->get();
        $users =new packageModel();
        $datae =$users->get();
        return view('wp-admin/your_pack',compact('data','datae'));
    }
    function see_pack($id){
    
        $user =new AdminModel();
        $data = $user->get();
        $user =DB::table('pack')
        ->join('package','pack.p_id', '=','package.p_id')
        ->join('sub_cat','pack.ids', '=','sub_cat.ids')
        ->where('pack.p_id',$id)
        ->get();
        return view('wp-admin/see_pack',compact('data','user'));
    }
    function pack_updates(Request $request,$p_id)
    {
        $use=packageModel::findOrFail($p_id);
        $use->p_name=$request->input('p_name');
        $use->save();
        $user = packModel::findOrFail($p_id);
       
        $user->delete();

        for( $i=0; $i<count($request->input('ids'));$i++)
        {
            $user = new packModel();
            $user->p_id = $request->p_id[$i]; 
            $user->p_name = $request->p_name; 
            $user->ids = $request->ids[$i];
            $user->pk_price = $request->pri[$i];
            $user->save(); 
        }
    return back()->with('success', ' Package Updated.');

    }
 
    function admin_do_login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|min:4|max:25',
            'password' => 'required|min:6',
        ]);

        if ($validator->fails()) {
            return redirect('/')
                        ->withErrors($validator)
                        ->withInput();
        }
        $user = AdminModel::where("email",$request->email)
                    ->where("password",$request->password)
                  
                    ->get();

        
    {
      
        if(!empty($user) && isset($user[0]->email))
            {
                if(!empty($user) && isset($user[0]->email) && $user[0]->state==0)
                    {
        
                        session(['email'=>$user[0]->email,
                        "id"=>$user[0]->id,
                        "add"=>$user[0]->addby,
                        "usertype"=>$user[0]->usertype
                        ]);
                        if(session("usertype")=='admin'){
                            return redirect("/wp-admin");
                        }
                        else if(session("usertype")=='reseller'){
                            return redirect("/re_index"); 
                        }
                        else if(session("usertype")=='user'){
                            return redirect("/dashboard"); 
                        }
                        else
                        {
                        return redirect('/');
                        }
                    } 
        
        
                else{
                    return back()
                    ->withErrors($validator)
                    ->withInput()
                    ->with('fail1','Whoops!your account disabled');

                }
            }
        else
        {
            return back()
            ->withErrors($validator)
            ->withInput()
            ->with('fail1','Whoops!Your email and paassword is not correct.');
        }
    }
    }
   
    function admin_logout()
  	{
  		session()->forget("email");
  		session()->flush();
  		return redirect("/");
  	}
 
}
