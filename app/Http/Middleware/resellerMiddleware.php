<?php

namespace App\Http\Middleware;

use Closure;

class resellerMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(session("usertype")=='reseller')
        {
            return $next($request);
            
        }
        return redirect("/");
      
        // return $next($request);
    }
}
